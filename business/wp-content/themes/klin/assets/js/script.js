// makes sure the whole site is loaded
jQuery(window).load(function () {
    // will first fade out the loading animation
    jQuery("#status").fadeOut();
    // will fade out the whole DIV that covers the website.
    jQuery("#preloader").delay(1000).fadeOut("slow");
})



function pickup() {
    $(".pickup").on("change paste keyup", function () {
        var pckp = $(this).val();
    })
}


function myFunction() {
    $("#field_7").on("change paste keyup", function () {
        var inpt = $(this).val();
        if (!$(".zone").hasClass(inpt)) {

            $("#result").html("Unfortunately your ZIP Code is not<br> in our delivery range yet. <br><a href=\"#coverage-map\" class=\"maplink\">Coverage Map</a>");
            $(".zone:not('" + inpt + "')").hide();
            //            $('#signup_submit').stop().fadeOut();
        } else if ($(".zone").hasClass(inpt)) {
            $("#result").html(" ");
            //            $('#signup_submit').stop().show();
            $(".zone." + inpt).fadeIn();
            $(".navi.sched").fadeIn();
            $(".details").fadeIn();
        }
    })
}

//  // Declare variables 
//  var input, filter, table, tr, td, i, ig;
//  input = document.getElementById("searchCodes");
//  filter = input.value.toUpperCase();
//  table = document.getElementById("codes");
//  tr = table.getElementsByTagName("span");
//    ig = table.getElementsByClassName("zone");
//  // Loop through all table rows, and hide those who don't match the search query
//  for (i = 0; i < tr.length; i++) {
//    td = tr[i].getElementsByTagName("p")[0];
//    if (td) {
//      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
//        document.getElementById("result").innerHTML = "Success";
//      } else if (td.innerHTML.toUpperCase().indexOf(filter) == 0) {
//        document.getElementById("result").innerHTML = "Failure"
//      } else {
//        document.getElementById("result").innerHTML = "Failure"
//      }
//    } 
//  }

$(document).ready(function () {
    $("#signup_email").hide();
    $("#signup_username").hide();
    $("#field_10").on("change paste keyup", function () {
        eml = $(this).val();
        $("#signup_email").val(eml);
        $("#signup_username").val(eml);
    });

    $("#field_7").attr("onkeyup", "myFunction()");
    $("#field_7").attr("autocomplete", "off");

    $('#user_login').attr('placeholder', 'Username');
    $('#user_pass').attr('placeholder', 'Password');
    $(function () {
        $('#fader img:not(:first)').hide();
        $('#fader img').css('position', 'absolute');
        $('#fader img').css('top', '0px');
        $('#fader img').css('top', '50vh');
        $('#fader img').each(function () {
            var img = $(this);
            $('<img>').attr('src', $(this).attr('src')).load(function () {
                img.css('margin-top', -this.width / 2 + 'px');
            });
        });

        var pause = false;

        function fadeNext() {
            $('#fader img').first().fadeOut().appendTo($('#fader'));
            $('#fader img').first().fadeIn();
        }

        function fadePrev() {
            $('#fader img').first().fadeOut();
            $('#fader img').last().prependTo($('#fader')).fadeIn();
        }

        //    $('#fader, #next').click(function() {
        //        fadeNext();
        //    });
        //
        //    $('#prev').click(function() {
        //        fadePrev();
        //    });
        //
        //    $('#fader, .button').hover(function() {
        //        pause = false;
        //    },function() {
        //        pause = true;
        //    });

        function doRotate() {
            if (!pause) {
                fadeNext();
            }
        }

        var rotate = setInterval(doRotate, 3000);
    });
    myFunction();



    $(".signin").click(function () {
        $('.login').fadeIn();
    });
    $(".cancel").click(function () {
        $('.login').fadeOut();
    });


    if (window.location.href.indexOf("login=failed") > -1) {
        $('.login').fadeIn();

    }



    /* SHOW/HIDE ZONES */
    $(".check").click(function () {
        if ($(this).hasClass("act")) {
            $(this).removeClass("act");
            $('.coverage').slideUp();
            $(this).html("Show Delivery Zones");
        } else {
            $(this).html("Hide Delivery Zones");
            $(this).addClass("act");
            $('.coverage').slideDown();
        }
    });
    /* END */


    /* HOW IT WORKS BUTTON */
    $(".service").click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).next('.items').slideUp();
            $('#pricing').css("height", "auto");
        } else {
            $('.items').slideUp();
            $('.service').removeClass('active');
            $(this).addClass('active');
            $(this).next('.items').slideDown();
        }
    });

    $(".top").click(function () {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $('html, body').offset().top
        }, 1500, "easeInOutCubic");
    });

    $(".menuorder").click(function () {
        event.preventDefault();
        $('.mobile nav.menu').removeClass('act');
        $('.mobile nav.menu').fadeOut();
        $('.mobile .menu-btn').html("Menu");
        $('html, body').animate({
            scrollTop: $($(".order")).offset().top - 80
        }, 1500, "easeInOutCubic");
    });
    /* END */

    /* CANCEL ORDER BUTTON */

    /* END */


//    $(function () {
        var $cog = $('#logo, #logom'),
            $body = $(document.body),
            bodyHeight = $body.height();

        $(window).scroll(function () {
            if ($(this).scrollTop() >= 100) { // if scroll is greater/equal then 100 and hasBeenTrigged is set to false.
                $('.desktop header.sticky').stop().slideDown(500, "easeInOutCubic");
            } else {
                $('.desktop header.sticky').slideUp(500, "easeInOutCubic");
                hasBeenTrigged = false;

            }
            $cog.css({
                'transform': 'rotate(' + ($body.scrollTop() / bodyHeight * 360) + 'deg)'
            });
        });
    
    
/* DATEPICKER & TIMES */   
        var dt = new Date();
        var time = dt.getHours();
var d = new Date();

var monthdate = d.getMonth()+1;
var daydate = d.getDate();
//    var dayplus = daydate;
    var dayplus = +daydate + 1;
var output =   (daydate<10 ? '0' : '') + dayplus + '/' +
    (monthdate<10 ? '0' : '') + monthdate + '/' + d.getFullYear();
//    alert(output + time);
    
//    alert(time);
        var pckd;
var pickupdates;
        $(".datepicker").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: 1,
            beforeShowDay: function (date) {
                day = date.getDay();
                return [(day != 0), ''];
            }
        });
var pickupday;
        $(".datepicker").on("change paste keyup", function () {
            var pickupdate = $(this).datepicker('getDate');
            var plustwo = new Date(pickupdate.getTime());
            plustwo.setDate(plustwo.getDate() + 2);
            pickupday = $.datepicker.formatDate('dd/mm/yy', pickupdate);
            $(".datepickertwo").datepicker("option", "minDate", plustwo);
//            alert(pickupdate + pickupday);
                if ( time > "16" && output == pickupday) {
       $(".700").parent().hide();
       $(".800").parent().hide();
       $(".900").parent().hide();
       $(".1000").parent().hide();
       $(".1100").parent().hide();
} else { 
           $(".700").parent().show();
           $(".800").parent().show();
           $(".900").parent().show();
           $(".1000").parent().show();
           $(".1100").parent().show();

};
        });
    
 
        $(".datepickertwo").datepicker({
            dateFormat: "dd/mm/yy",
            beforeShowDay: function (date) {
                var daytwo = date.getDay();
                if (pickupday == "Friday" || pickupday == "Saturday") {   
                return [(daytwo != 0 && daytwo != 1), ''];
                } else {
                return [(daytwo != 0), ''];
                }
            }
        });
        $(".datepickertwo").on("change paste keyup", function () {
//                    alert(pickupday);
        });
        $(".zone").hide();
        $(".onpkp").on("change", function () {
            var zip = $(this).attr("id");
            var date = $(this).datepicker('getDate');
            var day = $.datepicker.formatDate('DD', date);
            $(".zone." + zip).show();


            $(".tms-pkp").hide();
            $("." + day + "-pkp").show();
            $(this).css("background", "#A4CEC2");
        });
        //                $(".time").click(function() {
        //                    var tme = $(this).attr("name");
        //                    $(".time").removeClass("border");
        //                    $(this).addClass("border");
        //                });
        $(".ondel").on("change", function () {
            var zip = $(this).attr("id");
            var date = $(this).datepicker('getDate');
            var day = $.datepicker.formatDate('DD', date);
            $(".zone." + zip).show();

            $(".tms-del").hide();
            $("." + day + "-del").show();
            $(this).css("background", "#A4CEC2");
        });

        $(".menu-btn").click(function () {

            if ($('.mobile nav.menu').hasClass('act')) {
                $('.mobile nav.menu').removeClass('act');
                $('.mobile nav.menu').fadeOut();
                $(this).html("Menu");
            } else {
                $('.mobile nav.menu').addClass('act');
                $('.mobile nav.menu').fadeIn();
                $(this).html("Back");
            }
        });
        $("form").keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }


            });
        $("button.next-to-options").click(function () {
            event.preventDefault();
            $('.order').fadeOut(function () {
                $('.options').fadeIn();
                $('.dots.one').removeClass('act');
                $('.steps.one').removeClass('act');
                $('.dots.two').addClass('act');
                $('.steps.two').addClass('act');

            });
        });

        $("button.back-to-order").click(function () {
            event.preventDefault();
            $('.options').fadeOut(function () {
                $('.order').fadeIn();
                $('.dots.two').removeClass('act');
                $('.steps.two').removeClass('act');
                $('.dots.one').addClass('act');
                $('.steps.one').addClass('act');
            });
        });

        $("button.sched").click(function () {
            event.preventDefault();

            $('html, body').animate({
                scrollTop: $($(".order")).offset().top - 80
            }, 1500, "easeInOutCubic");
            $('.buttons-one, .details, #searchCodes').fadeOut(function () {
                $('.pckcont').fadeIn();
                $('.delcont').fadeIn();
                $('.buttons-two').fadeIn();
                $("form").keypress(
                    function (event) {
                        if (event.which == '13') {

                        }


                    });
            });

        });
        $("button.optprev").click(function () {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $($(".order")).offset().top - 80
            }, 1500, "easeInOutCubic");
            $('.buttons-two, .pckcont, .delcont').fadeOut(function () {
                $('.details').fadeIn();
                $('#searchCodes').fadeIn();
                $('.buttons-one').fadeIn();
            });

        });
        $("button.opt").click(function () {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $($(".order")).offset().top - 80
            }, 1500, "easeInOutCubic");
            $('.buttons-two, .pckcont, .delcont').fadeOut(function () {
                $('.shrtscont').fadeIn();
                $('.lndrycont').fadeIn();
                $('.lndryshrtscont').fadeIn();
                $('.buttons-three').fadeIn();
            });

        });
        $("button.schdprev").click(function () {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $($(".order")).offset().top - 80
            }, 1500, "easeInOutCubic", function () {

                $('.buttons-three, .shrtscont, .lndrycont, .lndryshrtscont').fadeOut(function () {
                    $('.buttons-two').fadeIn();
                    $('.pckcont').fadeIn();
                    $('.delcont').fadeIn();

                });
            });

        });
        $("button.schd").click(function () {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $($(".order")).offset().top - 80
            }, 1500, "easeInOutCubic", function () {

                $('.buttons-three, .shrtscont, .lndrycont, .lndryshrtscont').fadeOut(function () {
                    $('.message, .send').fadeIn();

                });
            });

        });

        $("button.sendprev").click(function () {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $($(".order")).offset().top - 80
            }, 1500, "easeInOutCubic", function () {

                $('.send, .message').fadeOut(function () {
                    $('.buttons-three').fadeIn();
                    $('.shrtscont, .lndrycont, lndryshrtscont').fadeIn();

                });
            });

        });
        $(".checkbox.pck").click(function () {
            if ($(this).hasClass("border")) {
                $(this).prop('checked', false);
                $(".checkbox.pck").removeClass("border");
            } else {
                $('.checkbox.pck').find('input:checkbox').prop('checked', false);
                $(".checkbox.pck").removeClass("border");
                $(this).addClass("border");
                $(this).find('input:checkbox').prop('checked', true);
            }
        });
        $(".checkbox.del").click(function () {
            if ($(this).hasClass("border")) {
                $(this).prop('checked', false);
                $(".checkbox.del").removeClass("border");
            } else {
                $('.checkbox.del').find('input:checkbox').prop('checked', false);
                $(".checkbox.del").removeClass("border");
                $(this).addClass("border");
                $(this).find('input:checkbox').prop('checked', true);
            }
        });
        $(".checkbox.opt").click(function () {
            if ($(this).hasClass("border")) {
                $(this).prop('checked', false);
                $(".checkbox.opt").removeClass("border");
            } else {
                $('.checkbox.opt').find('input:checkbox').prop('checked', false);
                $(".checkbox.opt").removeClass("border");
                $(this).addClass("border");
                $(this).find('input:checkbox').prop('checked', true);
            }
        });

        //                $("input").on('blur', function() {
        //                    if ($(this).val()) {
        //                        $(this).css("background-color", "#A4CEC2");
        //                    } else {
        //                        $(this).css("background-color", "#fff");
        //                    }
        //                });
//    });




    $(document).on('click', 'nav.menu a, .maplink', function (event) {
        event.preventDefault();
        $('.mobile nav.menu').removeClass('act');
        $('.mobile nav.menu').fadeOut();
        $('.mobile .menu-btn').html("Menu");
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 80
        }, 1500, "easeInOutCubic");
        window.location.hash = $(this).attr('href');

    });
    $(".tab_content_login").hide();
    $(".tabs_login a:first").addClass("active_login").show();
    $(".tab_content_login:first").show();
    $(".tabs_login a").click(function () {
        $(".tabs_login a").removeClass("active_login");
        $(this).addClass("active_login");
        $(".tab_content_login").hide();
        var activeTab = $(this).attr("href");
        //			if ($.browser.msie) {$(activeTab).show();}
        $(activeTab).show();
        return false;
    });
    
    
    
    
});
