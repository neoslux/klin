    <div class="bg" style="background-image:url('<?php the_field('bg', 129); ?>');"></div>

      <footer class="container-fluid darker">
                    <div class="row">
                        <div class="col-md-3 spacer">
                        </div>
                        <div class="col-md-3">
                            <h5>CONTACT US </h5>
                            <h4><a href="tel:<?php the_field('phone', 129); ?>"><?php the_field('phone', 129); ?></a><br>
                    <a href="mailto:<?php the_field('email', 129); ?>"><?php the_field('email', 129); ?></a></h4>
                       <div class="terms"><a href="#">Terms & Conditions</a></div>
                        </div>
                        <div class="col-md-3">
                            <h5>Supported by</h5>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/technoport.png" alt=""></div>
                        <div class="col-md-3 socials">
                            <h5>FOLLOW US ON</h5>
                            <?php if (get_field('facebook', 129)) : ?><a href="http://www.facebook.com/<?php the_field('facebook', 129); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_fb.png" alt=""></a>
                                <?php endif; ?>
                                    <?php if (get_field('instagram', 129)) : ?><a href="http://www.facebook.com/<?php the_field('instagram', 129); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_ig.png" alt=""></a>
                                        <?php endif; ?>
                                            <?php if (get_field('twitter', 129)) : ?><a href="http://www.facebook.com/<?php the_field('twitter', 129); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_tw.png" alt=""></a>
                                                <?php endif; ?>
                                                    <?php if (get_field('youtube', 129)) : ?><a href="http://www.facebook.com/<?php the_field('facebyoutubeook', 129); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_yt.png" alt=""></a>
                                                        <?php endif; ?>
                                                            <?php if (get_field('linkedin', 129)) : ?><a href="http://www.facebook.com/<?php the_field('linkedin', 129); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_li.png" alt=""></a>
                                                                <?php endif; ?>
                                                                    <?php if (get_field('vimeo', 129)) : ?><a href="http://www.facebook.com/<?php the_field('vimeo', 129); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_vi.png" alt=""></a>
                                                                        <?php endif; ?>
                        </div>
                    </div>
                </footer>
