<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
        <script src="https://use.typekit.net/bwu6bjg.js"></script>
        <script>
            try {
                Typekit.load({
                    async: true
                });
            } catch (e) {}

        </script>
        <style>
            h1, h2 { margin:0;}
    </style>
</head>

<body style="background-color:#fff;">

<div class="table" style="background:#fff; width:100%; height:100%; display:table;">
<div class="cell" style="background:#fff; width:100%; padding-top:80px; display:table-cell; vertical-align:top; text-align:center;">
<p><a href="http://www.klin.lu"><img src="assets/img/logo.png" width="150"></a></p>

<?php
//if(isset($_POST['email'])) {
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "me@cliff.lu";
    $email_watch = "me@cliff.lu";
    $email_subject = "Your email subject line";
 
    function died($error) {
        // your error code can go here
        echo "<p>We are very sorry, but there were error(s) found with the form you submitted.</p>";
        echo "<p>These errors appear below.</p><br /><br />";
        echo $error."<br /><br />";
        echo "<p>Please go back and fix these errors.</p><br /><br />";
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['pickup_address']) ||
        !isset($_POST['pickup_date']) ||
        !isset($_POST['last_name']) ||
        !isset($_POST['first_name']) ||
        !isset($_POST['email_from']) ||
        !isset($_POST['phone']) ||
        !isset($_POST['pickup_time']) ||
        !isset($_POST['delivery_date']) ||
        !isset($_POST['delivery_time']))
//        !isset($_POST['shirts']) ||
//        !isset($_POST['laundry']) ||
//        !isset($_POST['message']) ||
//        !isset($_POST['phone']) ||
//        !isset($_POST['comments'])) {
//        !isset($_POST['phone'])) 
    {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
 
     
 
    $last_name = $_POST['last_name']; // required
    $first_name = $_POST['first_name']; // required
    $email_from = $_POST['email_from']; // required
    $phone = $_POST['phone']; // required
    $pickup_address = $_POST['pickup_address']; // required
    $pickup_date = $_POST['pickup_date']; // required
    $pickup_time = $_POST['pickup_time']; // required
    $delivery_date = $_POST['delivery_date']; // required
    $delivery_time = $_POST['delivery_time']; // required
    $laundry = $_POST['laundry']; // required
    $special_details = $_POST['special_details']; // required
    $comments = $_POST['comments']; // required
//    $comments = $_POST['comments']; // required
 
    $error_message = "";

 
//  if(strlen($comments) < 2) {
//    $error_message .= 'The Comments you entered do not appear to be valid.<br />';
//  }
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
 
     
    function clean_string($string) {
      $bad = array("content-type","to:","href");
      return str_replace($bad,"",$string);
    }
 
     
 
    $email_message = '<html><body style="text-align:center">';
    $email_message .= '<img src="http://www.klin.lu/wp-content/themes/klin/img/logo.png" width="150"><br><br>';
    $email_message .= '<h1 style="#7DABA2">Order Summary</h1>';
    $email_message .= "Name: ".clean_string($first_name)." ".clean_string($last_name)."<br>";
    $email_message .= "Email: ".clean_string($email_from)."<br>";
    $email_message .= "phone: ".clean_string($phone)."<br>";
    $email_message .= "<h3>Address</h3>";
    $email_message .= "Address: ".clean_string($pickup_address)."<br>";
    $email_message .= "<h3>Schedule</h3>";
    $email_message .= "Pickup Date: ".clean_string($pickup_date)."<br>";
    $email_message .= "Pickup Time: ".clean_string($pickup_time)."<br>";
    $email_message .= "Delivery Date: ".clean_string($delivery_date)."<br>";
    $email_message .= "Delivery Time: ".clean_string($delivery_time)."<br>";
        if ( isset($_POST['laundry']) ) {
    $email_message .= "Laundry: ".clean_string($laundry)."<br>";
        }
    $email_message .= "Special Treatment: ".clean_string($special_details)."<br>";
    $email_message .= "Comments: ".clean_string($comments)."<br>";
 
// create email headers
$headers = 'From: '.$email_from."\r\n".
'MIME-Version: 1.0' . "\r\n".
'Content-type: text/html; charset=utf-8' . "\r\n".
'CC: '.$email_from."\r\n" .
'BCC: '.$email_watch."\r\n" .
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, "Klin.lu - Order", $email_message, $headers);  
?>
 

 <br><br>
    <h1><?php echo $first_name; ?></h1>
      <h2>Thank you for your order!</h2>            
      <br><br>
       <h4>Order Summary</h4>
    <p>Address: <?php echo $pickup_address; ?><br>
    
<p><strong>Contacts</strong><br>
    Email: <?php echo $email_from; ?><br>
    Phone: <?php echo $phone; ?></p><br><br>
    

<p>   <strong>Schedule</strong><br>
    Pickup Date: <?php echo $pickup_date; ?><br>
    Pickup Time: <?php echo $pickup_time; ?><br>
    Delivery Date: <?php echo $delivery_date; ?><br>
    Delivery Time: <?php echo $delivery_time; ?></p><br><br>
   <?php if ( $laundry == " " ) : ?>
<p><strong>Options</strong><br>
    Laundry: <?php echo $laundry; ?>
    </p>
    <?php endif; ?>
    <?php if ( $special_details == " " ) : ?>
    <p>
    Special Treatment: <?php echo $special_details; ?>
    </p>
    <?php endif; ?>
    <?php if ( $comments == " " ) : ?>
    <p>
    Comments: <?php echo $comments; ?>
    </p>
       <?php endif; ?>
        
    <p><a href="http://www.klin.lu/">Back to Klin</a></p><br><br>
 </div>
 </div>
                 <?php wp_footer(); ?>
</body>
<?php
 
//}
?>