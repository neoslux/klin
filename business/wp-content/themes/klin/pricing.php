<section id="pricing" class="container-full lighter">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <?php the_field('content_pricing'); ?>
                        </div>
                        <div class="col-md-2 col-lg-2 text-center"></div>
                        <div class="col-md-8 col-lg-8 text-center">
                            <div class="row" style="margin-top:20px;">
                                <div class="col-md-6 box text-left">
                                    <?php $args = array( 'post_type' => 'pricelist', 'posts_per_page' => 4, 'order' => 'asc' );
                                      $counter = 0;
$loop = new WP_Query( $args );

while ( $loop->have_posts() ) : $loop->the_post(); ?>

                                        <div class="inner">
                                            <div class="service">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/plus.png" class="plus">
                                                <h2><?php the_title(); ?></h2>
                                                <img src="<?php the_post_thumbnail_url(); ?>" class="serviceicon">
                                            </div>
                                            <div class="items">
                                                <?php if( have_rows('items') ):
                while ( have_rows('items') ) : the_row(); ?>
                                                    <p>
                                                        <?php the_sub_field('item'); ?> <span><?php the_sub_field('price'); ?> €</span></p>
                                                    <?php endwhile; endif; ?>
                                                        <?php if (get_field('note')) : ?>
                                                            <p class="note"><em><?php the_field('note'); ?></em></p>
                                                            <?php endif; ?>
                                            </div>
                                        </div>


                                        <?php endwhile; wp_reset_postdata(); ?>
                                </div>
                                <div class="col-md-6 box text-left">
                                    <?php $args = array( 'post_type' => 'pricelist', 'posts_per_page' => 4, 'offset' => 4, 'order' => 'asc' );
                                      $counter = 0;
$loop = new WP_Query( $args );

while ( $loop->have_posts() ) : $loop->the_post(); ?>

                                        <div class="inner">
                                            <div class="service">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/plus.png" class="plus">
                                                <h2><?php the_title(); ?></h2>
                                                <img src="<?php the_post_thumbnail_url(); ?>" class="serviceicon">
                                            </div>
                                            <div class="items">
                                                <?php if( have_rows('items') ):
                while ( have_rows('items') ) : the_row(); ?>
                                                    <p>
                                                        <?php the_sub_field('item'); ?> <span><?php the_sub_field('price'); ?> €</span></p>
                                                    <?php endwhile; endif; ?>
                                                        <?php if (get_field('note')) : ?>
                                                            <p class="note"><em><?php the_field('note'); ?></em></p>
                                                            <?php endif; ?>
                                            </div>
                                        </div>


                                        <?php endwhile; wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>