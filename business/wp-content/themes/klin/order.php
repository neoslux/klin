
                        <h1><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_order.png"> Make a new order</h1>
                        <div class="col-md-8 col-md-push-2">
                            <div class="timeline">
                                <div class="line"></div>
                                <div class="dots one act"></div>
                                <div class="steps one act">Step 1 - Schedule</div>
                                <div class="dots two"></div>
                                <div class="steps two">Step 2 - Options</div>
                                <div class="dots three"></div>
                                <div class="steps three">Step 3 - Payment</div>
                            </div>
                        </div>
                        <form name="contactform" method="post" action="http://dev.cliff.lu/klin/wp-content/themes/klin/send_form_email.php">


                            <div class="col-md-8 col-md-push-2 order">
                                <div class="col-md-6">

                            <?php 
                                global $bp;
                                $the_user_id = $bp->loggedin_user->userdata->ID;
                                $the_user_login = $bp->loggedin_user->userdata->user_login;
                                $address = bp_get_profile_field_data('field=Address&user_id='.bp_loggedin_user_id()); 
                                $zip = bp_get_profile_field_data('field=ZIP Code&user_id='.bp_loggedin_user_id()); 
                                $last_name = bp_get_profile_field_data('field=Last Name&user_id='.bp_loggedin_user_id()); 
                                $first_name = bp_get_profile_field_data('field=First Name&user_id='.bp_loggedin_user_id()); 
                                $email = bp_get_profile_field_data('field=Email&user_id='.bp_loggedin_user_id()); 
                                $phone = bp_get_profile_field_data('field=Phone&user_id='.bp_loggedin_user_id()); 
                            ?>
                                        <h5>Pickup</h5>
                                        <input type="hidden" id="last_name" name="last_name" value="<?php echo $last_name; ?>">
                                        <input type="hidden" id="first_name" name="first_name" value="<?php echo $first_name; ?>">
                                        <input type="hidden" id="email_from" name="email_from" value="<?php echo $email; ?>">
                                        <input type="hidden" id="phone" name="phone" value="<?php echo $phone; ?>">
                                        <input type="text" id="pickup_address" name="pickup_address" value="<?php echo $address; ?>, <?php echo $zip; ?>">
                                        <input type="text" placeholder="Choose date" class="datepicker onpkp" id="<?php echo $zip; ?>pkp" name="pickup_date">
                                        <?php $args = array( 'post_type' => 'zones', 'posts_per_page' => -1 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                            <div class="zone <?php echo str_replace('<br />', 'pkp ', get_field('zip_codes')) . " pkp "; ?>">


                                                <?php if( have_rows('delivery_pickup') ):
                while ( have_rows('delivery_pickup') ) : the_row(); ?>
                                                    <div class="col-md-12 text-center tms-pkp <?php the_sub_field('day'); ?>-pkp">
                                                        <?php if( have_rows('times') ):
                while ( have_rows('times') ) : the_row(); ?>
                                                            <div class="checkbox pck">
                                                                <input type="checkbox" id="pcktimeid" name="pickup_time" class="<?php echo str_replace(':', '', get_sub_field('from')); ?>" value="<?php the_sub_field('from'); ?> - <?php the_sub_field('to'); ?>">
                                                                <label>
                                                                    <?php the_sub_field('from'); ?> -
                                                                        <?php the_sub_field('to'); ?>
                                                                </label>
                                                            </div>



                                                            <?php endwhile; endif; ?>
                                                    </div>

                                                    <?php endwhile; endif; ?>
                                            </div>
                                            <?php endwhile; wp_reset_postdata(); ?>
                                </div>
                                <div class="col-md-6">
                                    <h5>Delivery</h5>
                                    <input type="text" id="delivery_address" name="delivery_address" value="<?php echo $address; ?>, <?php echo $zip; ?>">
                                    <input type="text" placeholder="Choose date" class="datepickertwo ondel" id="<?php echo $zip; ?>del" name="delivery_date">
                                    <?php $args = array( 'post_type' => 'zones', 'posts_per_page' => -1 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                        <div class="zone <?php echo str_replace('<br />', 'del ', get_field('zip_codes')) . " del "; ?>">

                                            <?php if( have_rows('delivery_pickup') ):
                while ( have_rows('delivery_pickup') ) : the_row(); ?>
                                                <div class="col-md-12 text-center tms-del <?php the_sub_field('day'); ?>-del">

                                                    <?php if( have_rows('times') ):
                while ( have_rows('times') ) : the_row(); ?>
                                                        <div class="checkbox del">
                                                            <input type="checkbox" id="deltimeid" name="delivery_time" value="<?php the_sub_field('from'); ?> - <?php the_sub_field('to'); ?>">
                                                            <label>
                                                                <?php the_sub_field('from'); ?> -
                                                                    <?php the_sub_field('to'); ?>
                                                            </label>
                                                        </div>

                                                        <?php endwhile; endif; ?>
                                                </div>
                                                <?php endwhile; endif; ?>

                                        </div>
                                        <?php endwhile; wp_reset_postdata(); ?>
                                </div>
                                <div class="col-md-8 col-md-push-2">
                                    <button class="next next-to-options">Next</button>
                                </div>
                            </div>
                            <div class="col-sm-10 col-md-8 col-sm-push-1 col-md-push-2 options">
                                <div class="col-md-6">
                                    <h5>Items by the kilo <a href="#">?<br></a>(T-shirts, socks, underwear, …)</h5>


                                    <div class="checkbox opt">
                                        <input type="checkbox" id="options_one" name="laundry" value="Wash & Fold">
                                        <label><img src="<?php echo get_template_directory_uri(); ?>/assets/img/options_wash.png"> <span>Wash & Fold<br><em>3€/kg</em></span>
                                        </label>
                                    </div>
                                    <div class="checkbox opt">
                                        <input type="checkbox" id="options_one" name="laundry" value="Iron & Fold">
                                        <label><img src="<?php echo get_template_directory_uri(); ?>/assets/img/options_iron.png"> <span>Iron & Fold<br><em>5€/kg</em></span>
                                        </label>
                                    </div>
                                    <div class="checkbox opt">
                                        <input type="checkbox" id="options_one" name="laundry" value="Wash, Iron & Fold">
                                        <label><img src="<?php echo get_template_directory_uri(); ?>/assets/img/options_all.png"> <span>Wash, Iron & Fold<br><em>7€/kg</em></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h5>Special Treatment Details</h5>
                                    <textarea type="text" id="special_details" name="special_details" placeholder="Please provide us with as many details as possible, so we can treat your stains adequately."></textarea>
                                    <h5>Delivery comments</h5>
                                    <textarea type="text" id="comments" name="comments" placeholder="Delivery at neighbours? Back door? Ring as special bell? Let us know here!"></textarea>

                                </div>
                                <div class="col-md-8 col-md-push-2">
                                    <button class="back back-to-order">Back</button>
<!--                                        <input type="submit" value="Submit Order">-->

                                    <script src="https://checkout.stripe.com/checkout.js"></script>

                                    <button id="customButton">Payment</button>

                                    <script>
                                        var handler = StripeCheckout.configure({
                                            key: 'pk_test_oYT1KCLqgreAJg0zdGAfjBNu',
                                            //  image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                                            locale: 'auto',
                                            panelLabel: 'Complete Order',
                                            token: function(token) {
                                                // You can access the token ID with `token.id`.
                                                // Get the token ID to your server-side code for use.
                                            }
                                        });

                                        document.getElementById('customButton').addEventListener('click', function(e) {
                                            // Open Checkout with further options:
                                            handler.open({
                                                name: 'Complete order',
                                                description: 'Charged after delivery.',
                                                zipCode: true
                                                    //    currency: 'eur'
                                                    //    amount: 2000
                                            });
                                            e.preventDefault();
                                        });

                                        // Close Checkout on page navigation:
                                        window.addEventListener('popstate', function() {
                                            handler.close();
                                        });

                                    </script>
                                 </div>
                            </div>
                            
            
                        </form>