  <section id="the-process" class="container-full">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php the_field('content_theprocess', 129); ?>
                        <div id="fader">
                            <?php 

$images = get_field('photos', 129);

if( $images ): ?>
                                <?php foreach( $images as $image ): ?>
                                    <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" data-scroll-speed="2" />
                                    <?php endforeach; ?>
                                        <?php endif; ?>
                        </div>
                </div>
            </div>
        </section>
        
       <script>
</script>