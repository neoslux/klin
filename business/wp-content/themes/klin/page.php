<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/manifest.json">
    <meta name="theme-color" content="#ffffff">
    <?php wp_head(); ?>
        <script src="https://use.typekit.net/bwu6bjg.js"></script>
        <script>
            try {
                Typekit.load({
                    async: true
                });
            } catch (e) {}

        </script>
</head>

<body>
    <div id="preloader">
        <div id="status"></div>
    </div>
                    <?php if ( is_page('klin') && is_user_logged_in() ): ?>
    <?php echo get_header(); ?>
        <?php else : ?>
    <?php echo get_header("account"); ?>
        <?php endif; ?>

        <!-- LOGIN FORM POPUP -->
        <div class="login">
            <div class="login-inner">
                <?php get_template_part( 'login' ); ?>
            </div>
        </div>

        <section id="intro" class="container-full">
            <div class="row">
                <div class="col-md-12 text-center">
                    <!-- ORDER FORM -->
                    <?php if ( is_page('klin') && is_user_logged_in() ): ?>
                                    <?php get_template_part( 'order' ); ?>

                        <?php endif; ?>
                            <!-- ORDER FORM END -->
                            <div class="col-md-12">



                                <?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
	 the_content(); 


	

		// End the loop.
		endwhile;
		?>


                            </div>
                </div>
            </div>
            <?php get_template_part( 'howitworks' ); ?>
        </section>
        <?php get_template_part( 'coveragemap' ); ?>
            <?php get_template_part( 'theprocess' ); ?>
                <?php get_template_part( 'pricing' ); ?>






                    <?php get_footer(); ?>
                        <?php wp_footer(); ?>
</body>

</html>
