                                      <?php if (get_locale() == 'en_GB') : ?>                                                       


                        <h1><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_order.png"> Make a new order</h1>
                                <?php endif; ?>
                                                              <?php if (get_locale() == 'fr_FR') : ?>                                                       
                        <h1><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_order.png"> Nouvelle commande</h1>

                                <?php endif; ?>
             
   <?php require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/klinv2/stripe/stripe-php/init.php';
 ?>
                               <?php if (get_locale() == 'en_GB') : ?>  

                        <form name="contactform" id="payment-form" method="post" action="https://www.klin.lu/business/wp-content/themes/klinv2/send_form_email.php">
                        <?php endif; ?>
                               <?php if (get_locale() == 'fr_FR') : ?>  
                               <form name="contactform" id="payment-form" method="post" action="https://www.klin.lu/business/wp-content/themes/klinv2/send_form_email_fr.php">
                        <?php endif; ?>



                            <?php 
                                global $bp;
                                $the_user_id = $bp->loggedin_user->userdata->ID;
                                $the_user_login = $bp->loggedin_user->userdata->user_login;
                                $address = bp_get_profile_field_data('field=Street name and number&user_id='.bp_loggedin_user_id()); 
                                $addressalt = bp_get_profile_field_data('field=Street name and number (alternate)&user_id='.bp_loggedin_user_id()); 
                                $zip = bp_get_profile_field_data('field=ZIP Code&user_id='.bp_loggedin_user_id()); 
                                $zipalt = bp_get_profile_field_data('field=ZIP Code (alternate)&user_id='.bp_loggedin_user_id()); 
                                $last_name = bp_get_profile_field_data('field=Last Name&user_id='.bp_loggedin_user_id()); 
                                $first_name = bp_get_profile_field_data('field=First Name&user_id='.bp_loggedin_user_id()); 
                                $email = bp_get_profile_field_data('field=Email&user_id='.bp_loggedin_user_id()); 
                                $phone = bp_get_profile_field_data('field=Phone&user_id='.bp_loggedin_user_id()); 
                                $code = bp_get_profile_field_data('field=Company Code&user_id='.bp_loggedin_user_id()); 
                                $company = bp_get_profile_field_data('field=Company Name&user_id='.bp_loggedin_user_id()); 
                            ?>
                                        <input type="hidden" id="last_name" name="last_name" value="<?php echo $last_name; ?>">
                                        <input type="hidden" id="first_name" name="first_name" value="<?php echo $first_name; ?>">
                                        <input type="hidden" id="email_from" name="email_from" value="<?php echo $email; ?>">
                                        <input type="hidden" id="phone" name="phone" value="<?php echo $phone; ?>">
                            
                               
                               
                               <div class="col-md-8 col-md-push-2">
                               <div class="row">
                                       <?php $args = array( 'post_type' => 'companies', 'posts_per_page' => -1 );
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                       <?php if( have_rows('details') ):
                    while ( have_rows('details') ) : the_row(); ?>
                    
                                   <?php if (strcasecmp(get_sub_field('code'), $code) == 0) : ?>
                                 <h4 class="boxed"><?php the_sub_field('company_name'); ?> - <?php the_sub_field('company_address'); ?></h4>
                                                         <div id="schedule">
                                                     <?php if( have_rows('schedule') ):
                                                        while ( have_rows('schedule') ) : the_row(); ?>
                                 
                                                          <div id="<?php the_sub_field('day'); ?><?php the_sub_field('company_address'); ?> - ">
                                                          </div>
                                 
                                                     <?php endwhile; endif; ?>
                                                          </div>
                                 <?php endif; ?>
                                 <?php endwhile; endif; ?>
                                 
                                  <?php endwhile; wp_reset_postdata(); ?>
                               </div>
                               </div>
                               <div class="col-md-8 col-md-push-2 order">
                                <div class="col-md-6">
                                      <?php if (get_locale() == 'en_GB') : ?>
                                        <h5>Pickup</h5>
                                        <?php endif; ?>
                                      <?php if (get_locale() == 'fr_FR') : ?>
                                        <h5>Collecte</h5>
                                        <?php endif; ?>
                                        
                                                             


                                     
                                                            
                                      <?php if (get_locale() == 'en_GB') : ?>                                                       
                                        <input type="text" placeholder="Choose date" class="datepickerthree datepkp" name="pickup_date" readonly="readonly" >
                                        <?php endif; ?>
                                      <?php if (get_locale() == 'fr_FR') : ?>                                                       
                                        <input type="text" placeholder="Choisir la date" class="datepickerthree datepkp" name="pickup_date" readonly="readonly">
                                        <?php endif; ?>
                                        
                 <?php $args = array( 'post_type' => 'companies', 'posts_per_page' => -1 );
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                   <?php if( have_rows('details') ):
                    while ( have_rows('details') ) : the_row(); ?>
                                   <?php if (strcasecmp(get_sub_field('code'), $code) == 0) : ?>
                                        <input type="hidden" id="" value="<?php echo $code; ?>" class="" name="company_code" readonly="readonly">
                                        <input type="hidden" id="" value="<?php the_sub_field('company_name'); ?>" class="" name="company" readonly="readonly">
                                        <input type="hidden" id="" value="<?php the_sub_field('company_address'); ?>" class="" name="company_address" readonly="readonly">

                             <?php if( have_rows('schedule') ):
                                while ( have_rows('schedule') ) : the_row(); ?>
                                
                                 <div class="checkbox timepkp <?php the_sub_field('day'); ?>">
                                        <input type="checkbox" class="pickup_time" name="pickup_time" id="" value="<?php the_sub_field('time'); ?>" data="<?php the_sub_field('day'); ?>" checked>
                                        <label>
                                            <?php the_sub_field('time'); ?>
                                        </label>
                                    </div>


                             <?php endwhile; endif; ?>
         <?php endif; ?>
         <?php endwhile; endif; ?>

          <?php endwhile; wp_reset_postdata(); ?>
 
                                                           

                                       
                            
                                </div>
                                <div class="col-md-6">
                                      <?php if (get_locale() == 'en_GB') : ?>
                                                                           <h5>Delivery</h5>
                                        <?php endif; ?>
                                      <?php if (get_locale() == 'fr_FR') : ?>
                                        <h5>Livraison</h5>
                                        <?php endif; ?>
                                    
                                                        <?php if (get_locale() == 'en_GB') : ?>                                                       
                                        <input type="text" placeholder="Choose date" class="datepickerthree datedel" name="delivery_date" readonly="readonly">
                                        <?php endif; ?>
                                      <?php if (get_locale() == 'fr_FR') : ?>                                                       
                                        <input type="text" placeholder="Choisir la date" class="datepickerthree datedel" name="delivery_date" readonly="readonly">
                                        <?php endif; ?>
                                        
                 <?php $args = array( 'post_type' => 'companies', 'posts_per_page' => -1 );
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                   <?php if( have_rows('details') ):
                    while ( have_rows('details') ) : the_row(); ?>
                                   <?php if (strcasecmp(get_sub_field('code'), $code) == 0) : ?>

                             <?php if( have_rows('schedule') ):
                                while ( have_rows('schedule') ) : the_row(); ?>
                                
                                 <div class="checkbox timedel <?php the_sub_field('day'); ?>">
                                        <input type="checkbox" class="delivery_time" name="delivery_time" id="" value="<?php the_sub_field('time'); ?>" data="<?php the_sub_field('day'); ?>" checked>
                                        <label>
                                            <?php the_sub_field('time'); ?>
                                        </label>
                                    </div>


                             <?php endwhile; endif; ?>
         <?php endif; ?>
         <?php endwhile; endif; ?>

          <?php endwhile; wp_reset_postdata(); ?>
                                </div>
               
                            </div>
                            <div class="col-sm-10 col-md-8 col-sm-push-1 col-md-push-2 opt">
                                
                                <?php if (get_locale() == 'en_GB') : ?>                                             

                                    <h5>Special Treatment Details</h5>
                                    <textarea type="text" id="special_details" name="special_details" placeholder="Please provide us with details, so we can treat your stains adequately."></textarea>
            
                             
                                                                    <?php endif; ?>
                                    <?php if (get_locale() == 'fr_FR') : ?>                                             

                                    <h5>Demande spécifique de traitement</h5>
                                    <textarea type="text" id="special_details" name="special_details" placeholder="Veuillez nous donner le plus de détails possibles quant à vos demandes spécifiques afin de traiter vos pièces en conséquence."></textarea>
                                   
                                                                    <?php endif; ?>

                            </div>
<!--                                        <input type="submit" value="Submit Order">-->
                            <div class="col-sm-10 col-md-8 col-sm-push-1 col-md-push-2 opt">
                               <?php if (get_locale() == 'en_GB') : ?>    
                                    <h5>PAYMENT</h5>
                                <?php endif; ?>
                                <?php if (get_locale() == 'fr_FR') : ?>    
                                    <h5>PAIEMENT</h5>
                                <?php endif; ?>


<style>
                                    

.StripeElement {
  background-color: white;
  padding: 14px 12px !important;
  border: 1px solid transparent;
    display: block;
    width:100%;
    position: relative;
    height:50px !important;
    margin:40px auto;
    }

.StripeElement--focus {
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}
        #intro .payment button { margin-top:30px;}
#intro .payment img { width:333px !important; height:50px; display:block; margin:15px auto;}
#intro .payment h5 { margin-bottom: 40px;}
    #intro .payment input[type="checkbox"] { opacity: 1 !important; position: relative !important;
vertical-align: top !important;
width: 20px !important;
padding: 0 !important;
bottom: auto !important;
top: auto !important;
opacity: 1 !important;
left: auto !important;
margin-top: 20px !important;
display: inline-block !important;}
    #intro .payment label {margin-top: 20px !important;}
                                    </style>
<script src="https://js.stripe.com/v3/"></script>

  <div class="form-row">
    <div id="card-element">
      <!-- a Stripe Element will be inserted here. -->
    </div>

    <!-- Used to display Element errors -->
    <div id="card-errors" role="alert"></div>
  </div>
                                                        <?php if (get_locale() == 'en_GB') : ?>    
                                    <h5>Your credit card will only be debited after you've received your order.</h5>
                                <?php endif; ?>
                                <?php if (get_locale() == 'fr_FR') : ?>    
                                    <h5>Votre carte sera débitée uniquement après que vous ayez récupéré vos affaires.</h5>
                                <?php endif; ?>

                               <?php if (get_locale() == 'en_GB') : ?> 
                                                                    
                                                            
                                                                       
                                                                              <input type="checkbox" id="acceptterms" name="acceptterms" value="Accept" class="terms">  
                                                                <label>
                                                                    I accept the <a href='https://www.klin.lu/business/terms-conditions' target='_blank'>terms and conditions.</a>
                                                                </label>                                                               
                                  
                           <img src="https://www.klin.lu/business/wp-content/themes/klinv2/assets/img/stripe_logo.png">

                                                                <?php endif; ?>
                               <?php if (get_locale() == 'fr_FR') : ?>  
  
                                                           
                                                                       
                                <input type="checkbox" id="acceptterms" name="acceptterms" value="Accept" class="terms">&nbsp;<label>
                                                                    J'accepte les <a href='https://www.klin.lu/businessterms-conditions' target='_blank'>Conditions Générales de Vente</a>
                                                                </label>
                                
                                                                <?php endif; ?>
                                    <?php if (get_locale() == 'en_GB') : ?>    
                                                                        <h5 class="errterms">Please accept our terms and conditions.</h5>

                           
  <p>
<!--<button class="back back-to-options">Back</button>-->
<button class="completeorder">Submit Order</button></p>
     <?php endif; ?>
                               <?php if (get_locale() == 'fr_FR') : ?>
                                                                    <h5 class="errterms">Merci d'accepter nos conditions générales.</h5>
   
                                 <p>
<button class="completeorder">Commander</button></p>
     <?php endif; ?>
                            </div>
                            
            
                        </form>