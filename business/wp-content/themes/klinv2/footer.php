    <div class="bg" style="background-image:url('<?php the_field('bg', 62); ?>');"></div>

      <footer class="container-fluid darker">
                    <div class="row">
                        <div class="col-md-3 spacer">
                        </div>
                        <div class="col-md-3">
                           <?php if (get_locale() == 'en_GB') : ?>
                            <h5>CONTACT US</h5>
                            <?php endif; ?>
                           <?php if (get_locale() == 'fr_FR') : ?>
                            <h5>CONTACTEZ-NOUS </h5>
                            <?php endif; ?>
                            <h4><a href="tel:<?php the_field('phone', 62); ?>"><?php the_field('phone', 62); ?></a><br>
                    <a href="mailto:<?php the_field('email', 62); ?>"><?php the_field('email', 62); ?></a></h4>
                           <?php if (get_locale() == 'en_GB') : ?>
                       <div class="terms"><a href="https://www.klin.lu/terms-conditions" target="_blank">Terms & Conditions</a></div>
                            <?php endif; ?>
                           <?php if (get_locale() == 'fr_FR') : ?>
                       <div class="terms"><a href="https://www.klin.lu/terms-conditions" target="_blank">Conditions générales de vente
</a></div>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-3 support">
                         <?php if (get_locale() == 'en_GB') : ?>
                            <h5>SUPPORTED BY</h5>
                            <?php endif; ?>
                           <?php if (get_locale() == 'fr_FR') : ?>
                            <h5>SOUTENU PAR</h5>
                            <?php endif; ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/technoport.png" alt=""></div>
                        <div class="col-md-3 socials">
                           <?php if (get_locale() == 'en_GB') : ?>
                            <h5>FOLLOW US</h5>
                            <?php endif; ?>
                           <?php if (get_locale() == 'fr_FR') : ?>
                            <h5>SUIVEZ-NOUS</h5>
                            <?php endif; ?>
                            <?php if (get_field('facebook', 62)) : ?><a href="http://www.facebook.com/<?php the_field('facebook', 62); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_fb.png" alt=""></a>
                                <?php endif; ?>
                                    <?php if (get_field('instagram', 62)) : ?><a href="http://www.instagram.com/<?php the_field('instagram', 62); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_ig.png" alt=""></a>
                                        <?php endif; ?>
                                            <?php if (get_field('twitter', 62)) : ?><a href="http://www.twitter.com/<?php the_field('twitter', 62); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_tw.png" alt=""></a>
                                                <?php endif; ?>
                                                    <?php if (get_field('youtube', 62)) : ?><a href="http://www.youtube.com/<?php the_field('facebyoutubeook', 62); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_yt.png" alt=""></a>
                                                        <?php endif; ?>
                                                            <?php if (get_field('linkedin', 62)) : ?><a href="http://www.linkedin.com/company/<?php the_field('linkedin', 62); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_li.png" alt=""></a>
                                                                <?php endif; ?>
                                                                    <?php if (get_field('vimeo', 62)) : ?><a href="http://www.vimeo.com/<?php the_field('vimeo', 62); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_vi.png" alt=""></a>
                                                                        <?php endif; ?>
                        </div>
                    </div>
                </footer>
