<?php /* Template Name: Terms & Conditions */ ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/manifest.json">
    <meta name="theme-color" content="#ffffff">
    <?php wp_head(); ?>
        <script src="https://use.typekit.net/bwu6bjg.js"></script>
        <script>
            try {
                Typekit.load({
                    async: true
                });
            } catch (e) {}

        </script>
          <script src="https://js.stripe.com/v3/"></script>                            
</head>

<body>
    <div id="preloader">
        <div id="status"></div>
    </div>
        
    <?php echo get_header("terms");?>
        

        <section id="terms" class="container-full">
            <div class="row">
                <div class="col-md-10 col-md-push-1 text-left">

<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		//
the_content();
        //
	} // end while
} // end if
?>

                            </div>
                
            </div>
            
        </section>






                    <?php get_footer('terms'); ?>
                        <?php wp_footer(); ?>
</body>

</html>
