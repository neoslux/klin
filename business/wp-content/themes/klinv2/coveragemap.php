<section id="coverage-map" class="container-full">

            <div class="row">
                <div class="col-md-12 text-center">
                                               <?php if (get_locale() == 'en_GB') : ?>

                    <?php the_field('content_coveragemap', 62); ?>
                    <?php endif; ?>
                                <?php if (get_locale() == 'fr_FR') : ?>

                    <?php the_field('content_coveragemap', 213); ?>
                    <?php endif; ?>


                        <div class="col-md-12 coverage">
                            <div class="row">
                                <div class="col-md-12">
                                                                   <?php if (get_locale() == 'en_GB') : ?>
                                    <h1>Delivery Zones</h1>
                                                                  <?php endif; ?>
                                                                   <?php if (get_locale() == 'fr_FR') : ?>
                                    <h1>Zones de livraison</h1>
                                                                  <?php endif; ?>
                                                                 </div>

                                <?php $args = array( 'post_type' => 'zones', 'posts_per_page' => -1, 'order' => 'asc' );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                    <div class="col-md-3 box">
                                        <div class="inner">
                                            <h4><?php the_field('description'); ?></h4>

                                        </div>
                                    </div>
                                    <?php endwhile; wp_reset_postdata(); ?>
                            </div>
                        </div>
                        <p>
                                                                     <?php if (get_locale() == 'en_GB') : ?>
                            <button class="check">Delivery Zones</button>
                                                                  <?php endif; ?>
                                                                   <?php if (get_locale() == 'fr_FR') : ?>
                            <button class="check">Zones de livraison</button>
                                                                  <?php endif; ?>
                        </p>
                        <div id="map" style="width:100%;height:550px;"></div>
                        <div id="capture"></div>
                        <script>
                            var map;
                            var src = 'https://www.klin.lu/wp-content/themes/klinv2/assets/map2.kml';

                            function initMap() {
                                map = new google.maps.Map(document.getElementById('map'), {
                                    center: new google.maps.LatLng(49.6075838, 6.0658306),
                                    zoom: 11,
                                    mapTypeId: 'roadmap',
                                    styles: [{
                                        "featureType": "administrative.country",
                                        "elementType": "geometry",
                                        "stylers": [{
                                            "lightness": 25
                                        }]
                                    }, {
                                        "featureType": "administrative.locality",
                                        "elementType": "labels",
                                        "stylers": [{
                                            "saturation": -100
                                        }, {
                                            "lightness": 55
                                        }]
                                    }, {
                                        "featureType": "road.arterial",
                                        "elementType": "labels",
                                        "stylers": [{
                                            "visibility": "off"
                                        }]
                                    }, {
                                        "featureType": "road.highway",
                                        "elementType": "geometry",
                                        "stylers": [{
                                            "saturation": -100
                                        }]
                                    }, {
                                        "featureType": "road.highway",
                                        "elementType": "labels",
                                        "stylers": [{
                                            "visibility": "off"
                                        }]
                                    }, {
                                        "featureType": "road.local",
                                        "elementType": "labels.text",
                                        "stylers": [{
                                            "visibility": "off"
                                        }]
                                    }]
                                });

                                var kmlLayer = new google.maps.KmlLayer(src, {
                                    suppressInfoWindows: true,
                                    preserveViewport: false,
                                    map: map
                                });
                                kmlLayer.addListener('click', function(event) {
                                    var content = event.featureData.infoWindowHtml;
                                    var testimonial = document.getElementById('capture');
                                    testimonial.innerHTML = content;
                                });
                            }
                        </script>



                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkajntZN-TCtNkwr92INAOIIwa04XS6cA&callback=initMap"></script>

                </div>
            </div>
        </section>