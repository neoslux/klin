<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
        <script src="https://use.typekit.net/bwu6bjg.js"></script>
        <script>
            try {
                Typekit.load({
                    async: true
                });
            } catch (e) {}

        </script>
        <style>
            h1, h2 { margin:0;}
    </style>
</head>

<body style="background-color:#fff;">

<div class="table" style="background:#fff; width:100%; height:100%; display:table;">
<div class="cell" style="background:#fff; width:100%; padding-top:80px; display:table-cell; vertical-align:top; text-align:center;">
<p><a href="https://www.klin.lu/business/klin-fr"><img src="assets/img/logo.png" width="150"></a></p>
   <?php require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/klinv2/stripe/stripe-php/init.php';
 ?>
<?php
$token = $_POST['stripeToken'];
$emailstripe = $_POST['email_from'];

\Stripe\Stripe::setApiKey("sk_live_GiLwj2PxW32CUYgXorr4Qn38");

// Token is created using Stripe.js or Checkout!
// Get the payment token ID submitted by the form:
// Create a Customer:
$customer = \Stripe\Customer::create(array(
  "email" => $emailstripe,
  "source" => $token,
));

$cus = $customer->id;
  //  echo $cus;
    ?>
<?php
//if(isset($_POST['email'])) {
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "business@klin.lu";
    $email_watch = "me@cliff.lu";
    $email_subject = "Your email subject line";
 
    function died($error) {
        // your error code can go here
        echo "<br><br><p>Nous sommes désolés, mais votre commande contient des erreurs.</p>";
        echo $error."<br /><br />";
        echo "<p><a href=\"https://www.klin.lu/business/klin-fr\">Réessayer.</a></p><br><br>";
        die();
    }
 
 
    // validation expected data exists
    if(
//        !isset($_POST['pickup_address']) ||
//        !isset($_POST['delivery_address']) ||
        !isset($_POST['company']) ||
        !isset($_POST['company_code']) ||
        !isset($_POST['company_address']) ||
        !isset($_POST['last_name']) ||
        !isset($_POST['first_name']) ||
        !isset($_POST['email_from']) ||
        !isset($_POST['phone']) ||
        !isset($_POST['pickup_date']) ||
        !isset($_POST['pickup_time']) ||
        !isset($_POST['delivery_date']) ||
        !isset($_POST['delivery_time']) ||
        !isset($_POST['acceptterms']))
//        !isset($_POST['shirts']) ||
//        !isset($_POST['laundry']) ||
//        !isset($_POST['message']) ||
//        !isset($_POST['phone']) ||
//        !isset($_POST['comments'])) {
    {
        died('Please check your form again.');       
    }
 
     
 
    $last_name = $_POST['last_name']; // required
    $first_name = $_POST['first_name']; // required
    $email_from = $_POST['email_from']; // required
    $company = $_POST['company']; // required
    $company_code = $_POST['company_code']; // required
    $company_address = $_POST['company_address']; // required
    $phone = $_POST['phone']; // required
    $pickup_date = $_POST['pickup_date']; // required
    $pickup_time = $_POST['pickup_time']; // required
    $delivery_date = $_POST['delivery_date']; // required
    $delivery_time = $_POST['delivery_time']; // required
//    $laundry = $_POST['laundry']; // required
    $special_details = $_POST['special_details']; // required
    $comments = $_POST['comments']; // required
    $stripe = $_POST['stripeToken']; // required
    $acceptterms = $_POST['acceptterms']; // required
//    $comments = $_POST['comments']; // required
 
    $error_message = "";

 
//  if(strlen($accpetterms) == "Accept") {
//    $error_message .= 'You have to accept our Terms & Conditions.<br />';
//  }
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
 
     
    function clean_string($string) {
      $bad = array("content-type","to:","href");
      return str_replace($bad,"",$string);
    }
 
     
 
    $email_message = '<html><body style="text-align:center">';
    $email_message .= '<img src="https://www.klin.lu/business/wp-content/themes/klinv2/assets/img/logo.png" width="150"><br><br>';
    $email_message .= '<h1 style="#7DABA2">Détails commande</h1>';
    $email_message .= "<p>ID Client:</p><h2>".clean_string($cus)."</h2>";
    $email_message .= "<p>".clean_string($stripe)."</p><br><br>";
    $email_message .= "Nom: ".clean_string($first_name)." ".clean_string($last_name)."<br>";
    $email_message .= "Email: ".clean_string($email_from)."<br>";
    $email_message .= "Téléphone: ".clean_string($phone)."<br>";
    $email_message .= "<h3>Adresse</h3>";
    $email_message .= "".clean_string($company_address)."<br>";
    $email_message .= "<h3>Horaire</h3>";
    $email_message .= "Date de collecte: ".clean_string($pickup_date)."<br>";
    $email_message .= "Heure de collecte: ".clean_string($pickup_time)."<br>";
    $email_message .= "Date de livraison: ".clean_string($delivery_date)."<br>";
    $email_message .= "Heure de livraison: ".clean_string($delivery_time)."<br><br>";
//    if ( $laundry == "") :
//    else :
//    $email_message .= "Laundry: ".clean_string($laundry)."<br>";
//    endif;
    if ( $special_details == "") :
    else :
    $email_message .= "Traîtement spécial: ".clean_string($special_details)."<br>";
    endif;

    $email_message .= "Accept Terms: ".clean_string($acceptterms)."<br>";
    
    $email_messagetwo = '<html><body style="text-align:center">';
    $email_messagetwo .= '<img src="https://www.klin.lu/business/wp-content/themes/klinv2/assets/img/logo.png" width="150"><br><br>';
    $email_messagetwo .= '<h1 style="#7DABA2">Détails commande</h1>';
    $email_messagetwo .= "Nom: ".clean_string($first_name)." ".clean_string($last_name)."<br>";
    $email_messagetwo .= "Email: ".clean_string($email_from)."<br>";
    $email_messagetwo .= "Téléphone: ".clean_string($phone)."<br>";
    $email_messagetwo .= "<h3>Adresse</h3>";
    $email_messagetwo .= "".clean_string($company_address)."<br>";
    $email_messagetwo .= "<h3>Horaire</h3>";
    $email_messagetwo .= "Date de collecte: ".clean_string($pickup_date)."<br>";
    $email_messagetwo .= "Heure de collecte: ".clean_string($pickup_time)."<br>";
    $email_messagetwo .= "Date de livraison: ".clean_string($delivery_date)."<br>";
    $email_messagetwo .= "Heure de livraison: ".clean_string($delivery_time)."<br><br>";
//    if ( $laundry == "") :
//    else :
//    $email_messagetwo .= "Laundry: ".clean_string($laundry)."<br>";
//    endif;
    if ( $special_details == "") :
    else :
    $email_messagetwo .= "Traîtement spécial: ".clean_string($special_details)."<br>";
    endif;

 
// create email headers
$headers = 'From: '.$email_from."\r\n".
'MIME-Version: 1.0' . "\r\n".
'Content-type: text/html; charset=utf-8' . "\r\n".
'BCC: '.$email_watch."\r\n" .
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, "Klin.lu Business - Order", $email_message, $headers);  
    
// create email headers
$headerstwo = 'From: '.$email_to."\r\n".
'MIME-Version: 1.0' . "\r\n".
'Content-type: text/html; charset=utf-8' . "\r\n".
'CC: '.$email_from."\r\n" .
'BCC: '.$email_watch."\r\n" .
'Reply-To: '.$email_to."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, "Klin.lu Business - Order", $email_messagetwo, $headerstwo);      
?>
 

 <br><br>
    <h1><?php echo $first_name; ?></h1>
      <h2>Merci!</h2>            
      <br><br>
       <h4>Détails commande</h4>
    <p><strong>Adresse:</strong><br><?php echo $company; ?> - <?php echo $company_address; ?></p><br>
    
<p><strong>Contacts:</strong><br>
    Email: <?php echo $email_from; ?><br>
    Téléphone: <?php echo $phone; ?></p><br>
    

<p>   <strong>Horaire:</strong><br>
    Date de collecte: <?php echo $pickup_date; ?><br>
    Heure de collecte: <?php echo $pickup_time; ?><br>
    Date de livraison: <?php echo $delivery_date; ?><br>
    Heure de livraison: <?php echo $delivery_time; ?></p><br>




    <?php if ( $special_details == "") :
    else : ?>
<p><strong>Traîtement spécial: </strong></p>
    <p>
    <?php echo $special_details; ?>
    </p><br>
   <?php endif; ?>




   

    <a href="https://www.klin.lu/business/klin-fr"><button>Retour vers Klin</button></a><br><br>
 </div>
 </div>
</body>
<?php
 
//}
?>