<div id="login-register-password">

	<?php global $user_ID, $user_identity; if (!$user_ID) { ?>


	<div class="tab_container_login">
		<div id="login" class="tab_content_login">

			<?php $register = $_GET['register']; $reset = $_GET['reset']; if ($register == true) { ?>
                                <?php if (get_locale() == 'en_GB') : ?>

			<h3>Success!</h3>
			<p>Check your email for the password and then return to log in.</p>
                               <?php endif; ?>
                                <?php if (get_locale() == 'fr_FR') : ?>
                                <h3>Succès!</h3>
			<p>Vérifiez votre boîte aux lettres pour le mot de passe et retournez vers cette page.</p>
                               <?php endif; ?>

			<?php } elseif ($reset == true) { ?>
                                <?php if (get_locale() == 'en_GB') : ?>

			<h3>Success!</h3>
			<p>Check your email to reset your password.</p>
                                                              <?php endif; ?>

                                <?php if (get_locale() == 'fr_FR') : ?>
                                	<h3>Succès!</h3>
			<p>Vérifiez votre boîte aux lettres pour réinitialiser le mot de passe.</p>
                               <?php endif; ?>

			<?php } else { ?>
                 <?php if (get_locale() == 'en_GB') : ?>
			<h2>Sign in</h2>

                                                              <?php endif; ?>

                                <?php if (get_locale() == 'fr_FR') : ?>
 			<h2>Se connecter</h2>

                               <?php endif; ?>

			<?php } ?>

			<form method="post" action="https://www.klin.lu/wp-login.php" class="wp-user-form">
				<div class="username">
				                                <?php if (get_locale() == 'en_GB') : ?>

					<label for="user_login"><?php _e('Username'); ?>: </label>
                                                                               <?php endif; ?>

					                                <?php if (get_locale() == 'fr_FR') : ?>
					<label for="user_login"><?php _e('Utilisateur'); ?>: </label>
                               <?php endif; ?>

					<input type="text" name="log" value="<?php echo esc_attr(stripslashes($user_login)); ?>" size="20" id="user_login" tabindex="11" />
				</div>
				<div class="password">
					<label for="user_pass"><?php _e('Password'); ?>: </label>
					<input type="password" name="pwd" value="" size="20" id="user_pass" tabindex="12" />
				</div>
				<div class="login_fields">
					<div class="rememberme">
						<label for="rememberme">
						   <?php if (get_locale() == 'en_GB') : ?>

							<input type="checkbox" name="rememberme" value="forever" checked="checked" id="rememberme" tabindex="13" /> Remember me
                                                                               <?php endif; ?>

					                                <?php if (get_locale() == 'fr_FR') : ?>
							<input type="checkbox" name="rememberme" value="forever" checked="checked" id="rememberme" tabindex="13" /> Se souvenir de moi 
                               <?php endif; ?>
						</label>
					</div>
					<?php do_action('login_form'); ?>
					<input type="submit" name="user-submit" value="<?php _e('Sign in'); ?>" tabindex="14" class="user-submit" />
					<input type="hidden" name="redirect_to" value="<?php echo esc_attr($_SERVER['REQUEST_URI']); ?>" />
					<input type="hidden" name="user-cookie" value="1" />
				</div>
			</form>
		</div>

		<div id="forgot" class="tab_content_login" style="display:none;">
								   <?php if (get_locale() == 'en_GB') : ?>

			<h2>Lost something?</h2>
			<p>Enter your email <br>to reset your password.</p>
			<?php endif; ?>
									   <?php if (get_locale() == 'fr_FR') : ?>

			<h2>Un oubli?</h2>
			<p>Entrer votre votre email <br>afin d’initialiser votre mot de passe</p>
			<?php endif; ?>
			<form method="post" action="<?php echo site_url('wp-login.php?action=lostpassword', 'login_post') ?>" class="wp-user-form">
				<div class="username">
					<label for="user_login" class="hide"><?php _e('Username or Email'); ?>: </label>
					<input type="text" name="user_login" value="" size="20" id="user_login" tabindex="1001" />
				</div>
				<div class="login_fields">
					<?php do_action('login_form', 'resetpass'); ?>
													   <?php if (get_locale() == 'en_GB') : ?>

					<input type="submit" name="user-submit" value="<?php _e('Reset my password'); ?>" class="user-submit" tabindex="1002" />
                                                   <?php endif; ?>
													   <?php if (get_locale() == 'fr_FR') : ?>
					<input type="submit" name="user-submit" value="<?php _e('Réinitialiser'); ?>" class="user-submit" tabindex="1002" />

                                                   <?php endif; ?>
					<?php $reset = $_GET['reset']; if($reset == true) { echo '<p>A message will be sent to your email address.</p>'; } ?>
					<input type="hidden" name="redirect_to" value="<?php echo esc_attr($_SERVER['REQUEST_URI']); ?>?reset=true" />
					<input type="hidden" name="user-cookie" value="1" />
				</div>
			</form>
		</div>
	</div>

	<?php } else { // is logged in ?>

	<div class="sidebox">
		<h3>Welcome, <?php echo $user_identity; ?></h3>
		<div class="usericon">
			<?php global $userdata; echo get_avatar($userdata->ID, 60); ?>

		</div>
		<div class="userinfo">
			<p>You&rsquo;re logged in as <strong><?php echo $user_identity; ?></strong></p>
			<p>
				<a href="<?php echo wp_logout_url('index.php'); ?>">Log out</a> | 
				<?php if (current_user_can('manage_options')) { 
					echo '<a href="' . admin_url() . '">' . __('Admin') . '</a>'; } else { 
					echo '<a href="' . admin_url() . 'profile.php">' . __('Profile') . '</a>'; } ?>

			</p>
		</div>
	</div>
	<?php } ?>
	<div class="tabs_login">
        <?php if (get_locale() == 'en_GB') : ?>
		    <a href="#login">Login</a> · <a href="#forgot">Forgot?</a> · <div class="cancel">Cancel</div>
       <?php endif; ?>
        <?php if (get_locale() == 'fr_FR') : ?>
		    <a href="#login">Login</a> · <a href="#forgot">Un oubli?</a> · <div class="cancel">Annuler</div>
       <?php endif; ?>
	</div>

</div>