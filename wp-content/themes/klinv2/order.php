                                      <?php if (get_locale() == 'en_GB') : ?>                                                       


                        <h1><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_order.png"> Make a new order</h1>
                                <?php endif; ?>
                                                              <?php if (get_locale() == 'fr_FR') : ?>                                                       
                        <h1><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_order.png"> Nouvelle commande</h1>

                                <?php endif; ?>
                        <div class="col-md-8 col-md-push-2">
                            <div class="timeline">
                                <div class="line"></div> 
                              <?php if (get_locale() == 'en_GB') : ?>
                                <div class="dots one act"></div>
                                <div class="steps one act">Step 1 - Schedule</div>
                                <div class="dots two"></div>
                                <div class="steps two">Step 2 - Options</div>
                                <div class="dots three"></div>
                                <div class="steps three">Step 3 - Payment</div>
                                <?php endif; ?>
                                                           <?php if (get_locale() == 'fr_FR') : ?>
                                                                                           <div class="dots one act"></div>
                                <div class="steps one act">Étape 1 - Horaire</div>
                                <div class="dots two"></div>
                                <div class="steps two">Étape 2 - Options</div>
                                <div class="dots three"></div>
                                <div class="steps three">Étape 3 - Paiement</div>

                                <?php endif; ?>
                            </div>
                        </div>
   <?php require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/klinv2/stripe/stripe-php/init.php';
 ?>
                               <?php if (get_locale() == 'en_GB') : ?>  

                        <form name="contactform" id="payment-form" method="post" action="https://www.klin.lu/wp-content/themes/klinv2/send_form_email.php">
                        <?php endif; ?>
                               <?php if (get_locale() == 'fr_FR') : ?>  
                               <form name="contactform" id="payment-form" method="post" action="https://www.klin.lu/wp-content/themes/klinv2/send_form_email_fr.php">
                        <?php endif; ?>


                            <div class="col-md-8 col-md-push-2 order">
                                <div class="col-md-6">

                            <?php 
                                global $bp;
                                $the_user_id = $bp->loggedin_user->userdata->ID;
                                $the_user_login = $bp->loggedin_user->userdata->user_login;
                                $address = bp_get_profile_field_data('field=Street name and number&user_id='.bp_loggedin_user_id()); 
                                $addressalt = bp_get_profile_field_data('field=Street name and number (alternate)&user_id='.bp_loggedin_user_id()); 
                                $zip = bp_get_profile_field_data('field=ZIP Code&user_id='.bp_loggedin_user_id()); 
                                $zipalt = bp_get_profile_field_data('field=ZIP Code (alternate)&user_id='.bp_loggedin_user_id()); 
                                $last_name = bp_get_profile_field_data('field=Last Name&user_id='.bp_loggedin_user_id()); 
                                $first_name = bp_get_profile_field_data('field=First Name&user_id='.bp_loggedin_user_id()); 
                                $email = bp_get_profile_field_data('field=Email&user_id='.bp_loggedin_user_id()); 
                                $phone = bp_get_profile_field_data('field=Phone&user_id='.bp_loggedin_user_id()); 
                            ?>
                                      <?php if (get_locale() == 'en_GB') : ?>
                                        <h5>Pickup</h5>
                                        <?php endif; ?>
                                      <?php if (get_locale() == 'fr_FR') : ?>
                                        <h5>Collecte</h5>
                                        <?php endif; ?>
                                        <input type="hidden" id="last_name" name="last_name" value="<?php echo $last_name; ?>">
                                        <input type="hidden" id="first_name" name="first_name" value="<?php echo $first_name; ?>">
                                        <input type="hidden" id="email_from" name="email_from" value="<?php echo $email; ?>">
                                        <input type="hidden" id="phone" name="phone" value="<?php echo $phone; ?>">
                                        
                                                                    <?php if ( $zipalt == "" ) : ?>
                                                                    <div class="checkbox border address">
                                                                <input type="checkbox" id="pickup_address" name="pickup_address" class="<?php echo $address; ?>" value="<?php echo $address; ?>, <?php echo $zip; ?>" checked>
                                                                <label>
                                                                    <?php echo $address; ?>, <?php echo $zip; ?>
                                                                </label>
                                                            </div>
                                      <?php if (get_locale() == 'en_GB') : ?>                                                            
                                    <p><a href="<?php echo get_edit_user_link( $current_user->ID );?>">Add alternate address</a></p>
                                        <?php endif; ?>
                                      <?php if (get_locale() == 'fr_FR') : ?>                                                            
                                    <p><a href="<?php echo get_edit_user_link( $current_user->ID );?>">Ajouter une adresse secondaire</a></p>
                                        <?php endif; ?>
                                                            <?php else : ?>
                                                            <div class="checkbox address">
                                                                <input type="checkbox" id="pickup_address" name="pickup_address" class="<?php echo $address; ?>" value="<?php echo $address; ?>, <?php echo $zip; ?>" data="<?php echo $zip; ?>">
                                                                <label>
                                                                    <?php echo $address; ?>, <?php echo $zip; ?>
                                                                </label>
                                                            </div>
                                                            <div class="checkbox address alt">
                                                                <input type="checkbox" id="pickup_address" name="pickup_address" class="<?php echo $addressalt; ?>" value="<?php echo $addressalt; ?>, <?php echo $zipalt; ?>" data="<?php echo $zipalt; ?>">
                                                                <label>
                                                                    <?php echo $addressalt; ?>, <?php echo $zipalt; ?>
                                                                </label>
                                                            </div>
                                                            <?php endif; ?>
                                      <?php if (get_locale() == 'en_GB') : ?>                                                       
                                        <input type="text" placeholder="Choose date" class="datepicker onpkp" id="<?php echo $zip; ?>pkp" name="pickup_date" readonly="readonly">
                                        <?php endif; ?>
                                      <?php if (get_locale() == 'fr_FR') : ?>                                                       
                                        <input type="text" placeholder="Choisir la date" class="datepicker onpkp" id="<?php echo $zip; ?>pkp" name="pickup_date" readonly="readonly">
                                        <?php endif; ?>
                                        <?php $args = array( 'post_type' => 'zones', 'posts_per_page' => -1 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                            <div class="zone <?php echo str_replace('<br />', 'pkp ', get_field('zip_codes')) . " pkp "; ?>">


                                                <?php if( have_rows('delivery_pickup') ):
                while ( have_rows('delivery_pickup') ) : the_row(); ?>
                                                    <div class="col-md-12 text-center tms-pkp <?php the_sub_field('day'); ?>-pkp">
                                                        <?php if( have_rows('times') ):
                while ( have_rows('times') ) : the_row(); ?>
                                                            <div class="checkbox pck">
                                                                <input type="checkbox" id="pcktimeid" name="pickup_time" class="<?php echo str_replace(':', '', get_sub_field('from')); ?>" value="<?php the_sub_field('from'); ?> - <?php the_sub_field('to'); ?>">
                                                                <label>
                                                                    <?php the_sub_field('from'); ?> -
                                                                        <?php the_sub_field('to'); ?>
                                                                </label>
                                                            </div>



                                                            <?php endwhile; endif; ?>
                                                    </div>

                                                    <?php endwhile; endif; ?>
                                            </div>
                                            <?php endwhile; wp_reset_postdata(); ?>
                                </div>
                                <div class="col-md-6">
                                      <?php if (get_locale() == 'en_GB') : ?>
                                                                           <h5>Delivery</h5>
                                        <?php endif; ?>
                                      <?php if (get_locale() == 'fr_FR') : ?>
                                        <h5>Livraison</h5>
                                        <?php endif; ?>
                                    
                                                            <?php if ( $zipalt == "" ) : ?>
                                                            <div class="checkbox border deladdress">
                                                                <input type="checkbox" id="delivery_address" name="delivery_address" class="<?php echo $address; ?>" value="<?php echo $address; ?>, <?php echo $zip; ?>" checked>
                                                                <label>
                                                                    <?php echo $address; ?>, <?php echo $zip; ?>
                                                                </label>
                                                            </div>                                     
                                      <?php if (get_locale() == 'en_GB') : ?>
                                                                <p><a href="<?php echo get_edit_user_link( $current_user->ID );?>">Add alternate address</a></p>
                                        <?php endif; ?>
                                      <?php if (get_locale() == 'fr_FR') : ?>
                                                                <p><a href="<?php echo get_edit_user_link( $current_user->ID );?>">Ajouter une adresse secondaire</a></p>
                                        <?php endif; ?>
                                                            <?php else : ?>
                                                            <div class="checkbox deladdress">
                                                                <input type="checkbox" id="delivery_address" name="delivery_address" class="<?php echo $address; ?>" value="<?php echo $address; ?>, <?php echo $zip; ?>">
                                                                <label>
                                                                    <?php echo $address; ?>, <?php echo $zip; ?>
                                                                </label>
                                                            </div>
                                                            <div class="checkbox deladdress alt">
                                                                <input type="checkbox" id="delivery_address" name="delivery_address" class="<?php echo $addressalt; ?>" value="<?php echo $addressalt; ?>, <?php echo $zipalt; ?>">
                                                                <label>
                                                                    <?php echo $addressalt; ?>, <?php echo $zipalt; ?>
                                                                </label>
                                                            </div>
                                                            <?php endif; ?>
                                      <?php if (get_locale() == 'en_GB') : ?>                                                       
                                    <input type="text" placeholder="Choose date" class="datepickertwo ondel" id="<?php echo $zip; ?>del" name="delivery_date" readonly="readonly">
                                     <?php endif; ?>
                                      <?php if (get_locale() == 'fr_FR') : ?>                                                       
                                    <input type="text" placeholder="Choisir la date" class="datepickertwo ondel" id="<?php echo $zip; ?>del" name="delivery_date" readonly="readonly">
                                     <?php endif; ?>
                                    <?php $args = array( 'post_type' => 'zones', 'posts_per_page' => -1 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                        <div class="zone <?php echo str_replace('<br />', 'del ', get_field('zip_codes')) . " del "; ?>">

                                            <?php if( have_rows('delivery_pickup') ):
                while ( have_rows('delivery_pickup') ) : the_row(); ?>
                                                <div class="col-md-12 text-center tms-del <?php the_sub_field('day'); ?>-del">

                                                    <?php if( have_rows('times') ):
                while ( have_rows('times') ) : the_row(); ?>
                                                        <div class="checkbox del">
                                                            <input type="checkbox" id="deltimeid" name="delivery_time" value="<?php the_sub_field('from'); ?> - <?php the_sub_field('to'); ?>">
                                                            <label>
                                                                <?php the_sub_field('from'); ?> -
                                                                    <?php the_sub_field('to'); ?>
                                                            </label>
                                                        </div>

                                                        <?php endwhile; endif; ?>
                                                </div>
                                                <?php endwhile; endif; ?>

                                        </div>
                                        <?php endwhile; wp_reset_postdata(); ?>
                                </div>
                                <div class="col-md-8 col-md-push-2">
                                                                         <?php if (get_locale() == 'en_GB') : ?>                                                       
                                    <h5 class="err">Please provide the required details.</h5>
                                    <button class="next next-to-options">Next</button>
                                                                        <?php endif; ?>
                                                                         <?php if (get_locale() == 'fr_FR') : ?>                                                       
                                    <h5 class="err">Merci de fournir tous les détails.</h5>
                                                                       <button class="next next-to-options">Suivant</button>
                                                                        <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-sm-10 col-md-8 col-sm-push-1 col-md-push-2 options">
                                
                                <?php if (get_locale() == 'en_GB') : ?>                                             

                                <div class="col-md-6">
                                    <h5>Special Treatment Details</h5>
                                    <textarea type="text" id="special_details" name="special_details" placeholder="Please provide us with details, so we can treat your stains adequately."></textarea>
                                </div>
                                <div class="col-md-6">
                                    <h5>Delivery comments</h5>
                                    <textarea type="text" id="comments" name="comments" placeholder="Delivery at neighbours? Back door? Ring a special bell? Digicode? Let us know here!"></textarea>

                                </div>
                                <div class="col-md-8 col-md-push-2">
                                    <button class="back back-to-order">Back</button>
                                    <button class="next next-to-payment">Next</button>
                                </div>
                                                                    <?php endif; ?>
                                    <?php if (get_locale() == 'fr_FR') : ?>                                             

                                <div class="col-md-6">
                                    <h5>Demande spécifique de traitement</h5>
                                    <textarea type="text" id="special_details" name="special_details" placeholder="Veuillez nous donner le plus de détails possibles quant à vos demandes spécifiques afin de traiter vos pièces en conséquence."></textarea>
                                </div>
                                   <div class="col-md-6">
                                    <h5>Commentaire livraison</h5>
                                    <textarea type="text" id="comments" name="comments" placeholder="Livrer chez vos voisins? Porte arrière? Digicode? Faites-le nous savoir!"></textarea>

                                </div>
                                <div class="col-md-8 col-md-push-2">
                                    <button class="back back-to-order">Retour</button>
                                    <button class="next next-to-payment">Suivant</button>
                                </div>
                                                                    <?php endif; ?>

                            </div>
<!--                                        <input type="submit" value="Submit Order">-->
                            <div class="col-sm-10 col-md-8 col-sm-push-1 col-md-push-2 payment">
                               <?php if (get_locale() == 'en_GB') : ?>    
                                    <h5>Your credit card will only be debited after you've received your order.<br>
                                    You can also choose to pay at the door by card or cash.</h5>
                                <?php endif; ?>
                                <?php if (get_locale() == 'fr_FR') : ?>    
                                    <h5>Votre carte sera débitée uniquement après que vous ayez récupéré vos affaires, ou bien choisissez de payer lors de la livraison par carte ou en espèces.</h5>
                                <?php endif; ?>

                           <img src="https://www.klin.lu/wp-content/themes/klinv2/assets/img/stripe_logo.png">

<style>
                                    

.StripeElement {
  background-color: white;
  padding: 14px 12px !important;
  border: 1px solid transparent;
    display: block;
    width:100%;
    position: relative;
    height:50px !important;
    margin:40px auto;
    }

.StripeElement--focus {
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}
        #intro .payment button { margin-top:30px;}
#intro .payment img { width:333px !important; height:50px; display:block; margin:15px auto;}
#intro .payment h5 { margin-bottom: 40px;}
    #intro .payment input[type="checkbox"] { opacity: 1 !important; position: relative !important;
vertical-align: top !important;
width: 20px !important;
padding: 0 !important;
bottom: auto !important;
top: auto !important;
opacity: 1 !important;
left: auto !important;
margin-top: 20px !important;
display: inline-block !important;}
    #intro .payment label {margin-top: 20px !important;}
                                    </style>
<script src="https://js.stripe.com/v3/"></script>

  <div class="form-row">
    <div id="card-element">
      <!-- a Stripe Element will be inserted here. -->
    </div>

    <!-- Used to display Element errors -->
    <div id="card-errors" role="alert"></div>
  </div>
                                                        
                               <?php if (get_locale() == 'en_GB') : ?> 
                                                                    <div class="pymt-type">
                                                                     <div class="checkbox pymt">
                                                                <input type="checkbox" id="debitcard" name="debitcard" value="Debit my card" class="terms">
                                                                 
                                                                <label>
                                                                   Debit my Card
                                                                </label>
                                                        </div>
                                                                
                                                                <div class="checkbox pymt">
                                                                <input type="checkbox" id="payatdelivery" name="payatdelivery" value="Pay at Delivery" class="terms">
                                                                 
                                                                <label>
                                                                    I will pay at the door
                                                                </label>
                                                        </div></div>   
<!--
                                                                              <div class="brk">
                                                                               <input type="checkbox" id="payatdelivery" name="payatdelivery" value="Pay at Delivery" class="terms">
                                                                 
                                                                <label>
                                                                    I will pay at the door
                                                                </label>
                                                                </div>
-->
                                                                              <div class="brk">
                                                                       
                                                                              <input type="checkbox" id="acceptterms" name="acceptterms" value="Accept" class="terms">  
                                                                <label>
                                                                    I accept the <a href='https://www.klin.lu/terms-conditions' target='_blank'>terms and conditions.</a>
                                                                </label>                                                               
                                                                </div>
                                  

                                                                <?php endif; ?>
                               <?php if (get_locale() == 'fr_FR') : ?>  
  
                                                            
                                                                    <div class="pymt-type">
                                                                     <div class="checkbox pymt">
                                                                <input type="checkbox" id="debitcard" name="debitcard" value="Débiter ma carte" class="terms">
                                                                 
                                                                <label>
                                                                   Débiter ma carte
                                                                </label>
                                                        </div>
                                                                
                                                                <div class="checkbox pymt">
                                                                <input type="checkbox" id="payatdelivery" name="payatdelivery" value="Lors de la livraison" class="terms">
                                                                 
                                                                <label>
                                                                    Lors de la livraison
                                                                </label>
                                                        </div></div>   
                                                             
                                                              <div class="brk">
                                                                       
                                <input type="checkbox" id="acceptterms" name="acceptterms" value="Accept" class="terms">&nbsp;<label>
                                                                    J'accepte les <a href='https://www.klin.lu/terms-conditions' target='_blank'>Conditions Générales de Vente</a>
                                                                </label>
                                                               </div>
                                
                                                                <?php endif; ?>
                                    <?php if (get_locale() == 'en_GB') : ?>    
                                                                        <h5 class="errterms">Please accept our terms and conditions.</h5>

                           
  <p>
<button class="back back-to-options">Back</button>
<button class="completeorder">Submit Order</button></p>
     <?php endif; ?>
                               <?php if (get_locale() == 'fr_FR') : ?>
                                                                    <h5 class="errterms">Merci d'accepter nos conditions générales.</h5>
   
                                 <p>
<button class="back back-to-options">Retour</button>
<button class="completeorder">Commander</button></p>
     <?php endif; ?>
                            </div>
                            
            
                        </form>