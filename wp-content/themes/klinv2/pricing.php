<section id="pricing" class="container-full lighter">
                    <div class="row">
                        <div class="col-md-10 col-md-push-1 text-center">
                           <?php if (get_locale() == 'en_GB') : ?>
                            <?php the_field('content_pricing', 62); ?>
                    <?php endif; ?>
                                           <?php if (get_locale() == 'fr_FR') : ?>
                            <?php the_field('content_pricing', 213); ?>
                    <?php endif; ?>
                        </div>
                                                <div class="col-md-12"><br><br></div>

                        <div class="col-md-2 col-lg-2 text-center"></div>
                        <div class="col-md-8 col-lg-8 text-center">
                            <div class="row" style="margin-top:20px;">
                                <div class="col-md-6 box text-left">
                                    <?php $args = array( 'post_type' => 'pricelist', 'posts_per_page' => 5, 'order' => 'asc' );
                                      $counter = 0;
$loop = new WP_Query( $args );

while ( $loop->have_posts() ) : $loop->the_post(); ?>

                                        <div class="inner">
                                            <div class="service">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/plus.png" class="plus">
                                                <h2><?php the_title(); ?></h2>
                                                <img src="<?php the_post_thumbnail_url(); ?>" class="serviceicon">
                                            </div>
                                            <div class="items">
                                                <?php if( have_rows('items') ):
                while ( have_rows('items') ) : the_row(); ?>
                                                    <p>
                                                        <?php the_sub_field('item'); ?> <span><?php the_sub_field('price'); ?></span></p>
                                                    <?php endwhile; endif; ?>
                                                        <?php if (get_field('note')) : ?>
                                                            <p class="note"><em><?php the_field('note'); ?></em></p>
                                                            <?php endif; ?>
                                            </div>
                                        </div>


                                        <?php endwhile; wp_reset_postdata(); ?>
                                </div>
                                <div class="col-md-6 box text-left">
                                    <?php $args = array( 'post_type' => 'pricelist', 'posts_per_page' => 5, 'offset' => 5, 'order' => 'asc' );
                                      $counter = 0;
$loop = new WP_Query( $args );

while ( $loop->have_posts() ) : $loop->the_post(); ?>

                                        <div class="inner">
                                            <div class="service">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/plus.png" class="plus">
                                                <h2><?php the_title(); ?></h2>
                                                <img src="<?php the_post_thumbnail_url(); ?>" class="serviceicon">
                                            </div>
                                            <div class="items">
                                                <?php if( have_rows('items') ):
                while ( have_rows('items') ) : the_row(); ?>
                                                    <p>
                                                        <?php the_sub_field('item'); ?> <span><?php the_sub_field('price'); ?></span></p>
                                                    <?php endwhile; endif; ?>
                                                        <?php if (get_field('note')) : ?>
                                                            <p class="note"><em><?php the_field('note'); ?></em></p>
                                                            <?php endif; ?>
                                            </div>
                                        </div>


                                        <?php endwhile; wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12"><br><br></div>
                            <div class="col-md-10 col-md-push-1 text-center">
                                                          <?php if (get_locale() == 'en_GB') : ?>
                                <h4><?php the_field('note_pricing', 62); ?></h4>
                                <?php endif; ?>
                                                          <?php if (get_locale() == 'fr_FR') : ?>
                                <h4><?php the_field('note_pricing', 213); ?></h4>
                                <?php endif; ?>
                            </div>
                    </div>
                </section>