<?php if ( is_user_logged_in() && is_page('sign-up') ) {
    wp_redirect('klin');
    exit;
}?>
<?php if ( is_user_logged_in() && is_page('sign-up-fr') ) {
    wp_redirect('klin-fr');
    exit;
}?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/manifest.json">
    <meta name="theme-color" content="#ffffff">
    <?php wp_head(); ?>
        <script src="https://use.typekit.net/bwu6bjg.js"></script>
        <script>
            try {
                Typekit.load({
                    async: true
                });
            } catch (e) {}

        </script>
          <script src="https://js.stripe.com/v3/"></script> 
                     
</head>

<body>
   <div class="currlang" id="<?php echo pll_current_language(locale); ?>" style="width:0; height:0;"></div>
    <div id="preloader">
        <div id="status"></div>
    </div>
                    <?php if ( is_page('klin') && is_user_logged_in() ): ?>
    <?php echo get_header(); ?>
                    <?php elseif ( is_page('klin-fr') && is_user_logged_in() ): ?>
    <?php echo get_header('fr'); ?>
        <?php else : ?>
                                                   <?php if (get_locale() == 'en_GB') : ?>
    <?php echo get_header("account"); ?>
        <?php endif; ?>
                                                   <?php if (get_locale() == 'fr_FR') : ?>
    <?php echo get_header("account-fr"); ?>
        <?php endif; ?>
        <?php endif; ?>

        <!-- LOGIN FORM POPUP -->
        <div class="login">
            <div class="login-inner">
                <?php get_template_part( 'login' ); ?>
            </div>
        </div>

        <section id="intro" class="container-full">
            <div class="row">
                <div class="col-md-12 text-center">
                    <!-- ORDER FORM -->
                    <?php if ( is_page('klin') || is_page('klin-fr') ): ?>
                                    <?php get_template_part( 'order' ); ?>
                        <?php endif; ?>
                                      <?php if ( is_page('sign-up') || is_page('sign-up-fr') ): ?>
                                    <?php get_template_part( 'signup' ); ?>
                        <?php endif; ?>
                   
                            <!-- ORDER FORM END -->
                            <div class="col-md-12">



                                <?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
	 the_content(); 


	

		// End the loop.
		endwhile;
		?>


                            </div>
                </div>
            </div>
            <?php get_template_part( 'howitworks' ); ?>
        </section>
                        <?php get_template_part( 'pricing' ); ?>

        <?php get_template_part( 'coveragemap' ); ?>
            <?php get_template_part( 'theprocess' ); ?>






                    <?php get_footer(); ?>
                        <?php wp_footer(); ?>
                                        <?php if ( is_page('sign-up-fr') ): ?>
	               <script type="text/javascript">

  var holder = $("input#field_5").attr('placeholder','Code postal');
  var holder = $("input#field_6").attr('placeholder','Adresse (rue et n°)');
  var holder = $("input#field_1").attr('placeholder','Prénom');
  var holder = $("input#field_2").attr('placeholder','Nom');
  var holder = $("input#field_4").attr('placeholder','Téléphone');
  var holder = $("input#field_3").attr('placeholder','Email');
  var holder = $("input#signup_password").attr('placeholder','Mot de passe');
  var holder = $("input#signup_password_confirm").attr('placeholder','Confirmer mot de passe');
  var holder = $("input#signup_submit").attr('Inscrire');
       
                       </script>
                        <?php endif; ?>      
</body>

</html>
