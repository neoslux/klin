  <section id="the-process" class="container-full">
            <div class="row">
                <div class="col-md-12 text-center">
                   <?php if (get_locale() == 'en_GB') : ?>
                    <?php the_field('content_theprocess', 62); ?>
                    <?php endif; ?>
                       <?php if (get_locale() == 'fr_FR') : ?>
                    <?php the_field('content_theprocess', 213); ?>
                    <?php endif; ?>
                        <div id="fader">
                            <?php 

$images = get_field('photos', 62);

if( $images ): ?>
                                <?php foreach( $images as $image ): ?>
                                    <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" data-scroll-speed="2" />
                                    <?php endforeach; ?>
                                        <?php endif; ?>
                        </div>
                </div>
            </div>
        </section>
        
       <script>
</script>