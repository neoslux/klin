  <div class="row" id="how-it-works">
                <div class="col-md-12 text-center">
                                             <?php if (get_locale() == 'en_GB') : ?>
                                <?php the_field('content_howitworks', 62); ?>
                                <?php endif; ?>
                                      <?php if (get_locale() == 'fr_FR') : ?>
                                <?php the_field('content_howitworks', 213); ?>
                                <?php endif; ?>
                    
                </div>
                                             <?php if (get_locale() == 'en_GB') : ?>

                <?php if( have_rows('step', 62) ):
                while ( have_rows('step', 62) ) : the_row(); ?>
                    <div class="col-sm-6 col-lg-3 box">
                        <div class="inner">
                            <img src="<?php the_sub_field('icon'); ?>" alt="">
                            <h3><?php the_sub_field('title'); ?></h3>
                            <p>
                                <?php the_sub_field('content'); ?>
                            </p>
                            <!--                            <button>Read More</button>-->
                        </div>
                    </div>
                    <?php endwhile; endif; ?>
                    <?php endif; ?>
                                                <?php if (get_locale() == 'fr_FR') : ?>

                <?php if( have_rows('step', 213) ):
                while ( have_rows('step', 213) ) : the_row(); ?>
                    <div class="col-sm-6 col-lg-3 box">
                        <div class="inner">
                            <img src="<?php the_sub_field('icon'); ?>" alt="">
                            <h3><?php the_sub_field('title'); ?></h3>
                            <p>
                                <?php the_sub_field('content'); ?>
                            </p>
                            <!--                            <button>Read More</button>-->
                        </div>
                    </div>
                    <?php endwhile; endif; ?>
                    <?php endif; ?>


            </div>
                                               
                         