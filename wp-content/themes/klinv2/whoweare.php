        <?php if(get_field("content_whoweare")) : ?>
            <section id="who-we-are" class="container-full light">
                <div class="row">
                    <div class="col-md-12 text-center">
                       	                              <?php if (get_locale() == 'en_GB') : ?>
                        <?php the_field('content_whoweare', 129); ?>
                                                        <?php endif; ?>
                       	                              <?php if (get_locale() == 'fr_FR') : ?>
                        <?php the_field('content_whoweare', 213); ?>
                                                        <?php endif; ?>
                    </div>
                                  <?php endif; ?>
                       	                              <?php if (get_locale() == 'en_GB') : ?>
                                                             <?php if( have_rows('members', 129) ):
                while ( have_rows('members', 129) ) : the_row(); ?>
                        <div class="col-md-4 box text-center">
                            <div class="inner">
                                <img src="<?php the_sub_field('picture_member'); ?>" alt="">
                                <?php the_sub_field('content_member'); ?>
                            </div>
                        </div>
                        <?php endwhile; endif; ?>
                                                        <?php endif; ?>
                       	                              <?php if (get_locale() == 'fr_FR') : ?>
                              <?php if( have_rows('members', 213) ):
                while ( have_rows('members', 213) ) : the_row(); ?>
                        <div class="col-md-4 box text-center">
                            <div class="inner">
                                <img src="<?php the_sub_field('picture_member'); ?>" alt="">
                                <?php the_sub_field('content_member'); ?>
                            </div>
                        </div>
                        <?php endwhile; endif; ?>
                                                        <?php endif; ?>
              


                </div>
            </section>
            <?php endif; ?>