<div class="desktop">
       <header class="container-full sticky">
        <div class="row">
            <div class="col-md-8">
            <a href="https://www.klin.lu"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" id="logo"></a>
            <nav class="menu">
                    <?php wp_nav_menu(''); ?>
                </nav>
            </div>
            <div class="col-md-4 text-right">
              <nav class="contact">

                        
                            <?php
$current_user = wp_get_current_user();
if ( 0 == $current_user->ID ) { ?>
                    
                    <button class="signin">Sign In</button>

<?php } else { ?>
    <?php
echo 'Welcome back,  ' . $current_user->user_firstname;
                    ?>  <a href="<?php echo get_edit_user_link( $current_user->ID );?>"><button>My Account</button></a><a id="wp-submit" href="<?php echo wp_logout_url(); ?>" title="Logout" class="logout">
    </a>
<?php } ?>

                </nav>

            </div>
        </div>
    </header>
    <header class="container-full">
           <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-8 col-xl-6 text-left">
            <a href="https://www.klin.lu"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" id="logo"></a>

                <nav class="menu">
        <?php wp_nav_menu(''); ?>
                </nav>
               </div>
            <div class="col-md-12 col-md-4 col-lg-4 col-xl-6 text-right">
                <nav class="contact">
                <p class="lang"><a href="https://www.klin.lu/">EN</a> | <a href="https://www.klin.lu/klin-fr">FR</a></p>

                        
                            <?php
$current_user = wp_get_current_user();
if ( 0 == $current_user->ID ) { ?>

                    <button class="signin">Sign In</button>

<?php } else { ?>
    <?php
echo 'Welcome back,  ' . $current_user->user_firstname;
                    ?>  <a href="<?php echo get_edit_user_link( $current_user->ID );?>"><button>My Account</button></a><a id="wp-submit" href="<?php echo wp_logout_url(); ?>" title="Logout" class="logout">
    </a>
<?php } ?>

             <p><a href="tel:<?php the_field('phone', 62); ?>"><?php the_field('phone', 62); ?></a> | 
                            <a href="mailto:<?php the_field('email', 62); ?>"><?php the_field('email', 62); ?></a>        </p>
                </nav>

            </div>
        </div>
    </header>
    </div>
       <div class="mobile">
       <header class="container-full">
        <div class="row">
            <div class="col-xs-3">
            <a href="https://www.klin.lu"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" id="logo"></a>
            
            </div>
            <div class="col-xs-9 text-right">
                                        
                                         <?php
$current_user = wp_get_current_user();
if ( 0 == $current_user->ID ) { ?>
                    
                    <button class="signin">Sign In</button>

<?php } else { ?>
    <?php
echo 'Welcome back,  ' . $current_user->user_firstname;
                    ?> <a id="wp-submit" href="<?php echo wp_logout_url(); ?>" title="Logout" class="logout"></a>
<?php } ?>
<div class="menu-btn">MENU</div>
            </div>
        </div>
    </header>
    <nav class="menu">

                   <div class="inner">

                                                                                          <?php
$current_user = wp_get_current_user();
if ( 0 == $current_user->ID ) { ?>
                    
                <p><a href="https://www.klin.lu/sign-up">EN</a> | <a href="https://www.klin.lu/sign-up-fr">FR</a></p>
                    <button class="signin">Sign In</button>

<?php } else { ?>
                <p><a href="https://www.klin.lu/klin">EN</a> | <a href="https://www.klin.lu/klin-fr">FR</a></p>
    <?php
echo 'Welcome back,  ' . $current_user->user_firstname;
                       ?>  <a href="<?php echo get_edit_user_link( $current_user->ID );?>"><button>My Account</button></a>
<?php } ?>
                    <?php wp_nav_menu(''); ?>
                            <?php if (get_field('facebook', 62)) : ?><a href="https://www.facebook.com/<?php the_field('facebook', 62); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_fb.png" alt=""></a>
                                <?php endif; ?>
                                    <?php if (get_field('instagram', 62)) : ?><a href="https://www.facebook.com/<?php the_field('instagram', 62); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_ig.png" alt=""></a>
                                        <?php endif; ?>
                                            <?php if (get_field('twitter', 62)) : ?><a href="https://www.facebook.com/<?php the_field('twitter', 62); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_tw.png" alt=""></a>
                                                <?php endif; ?>
                                                    <?php if (get_field('youtube', 62)) : ?><a href="https://www.facebook.com/<?php the_field('facebyoutubeook', 62); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_yt.png" alt=""></a>
                                                        <?php endif; ?>
                                                            <?php if (get_field('linkedin', 62)) : ?><a href="https://www.facebook.com/<?php the_field('linkedin', 62); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_li.png" alt=""></a>
                                                                <?php endif; ?>
                                                                    <?php if (get_field('vimeo', 62)) : ?><a href="https://www.facebook.com/<?php the_field('vimeo', 62); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/social_vi.png" alt=""></a>
                                                                        <?php endif; ?>
                    </div>
                </nav>
</div>