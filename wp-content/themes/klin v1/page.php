<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/manifest.json">
<meta name="theme-color" content="#ffffff">
    <?php wp_head(); ?>
        <script src="https://use.typekit.net/bwu6bjg.js"></script>
        <script>
            try {
                Typekit.load({
                    async: true
                });
            } catch (e) {}

        </script>
</head>

<body>
    <div id="preloader">
        <div id="status"></div>
    </div>

    <?php echo get_header(); ?>


        <section id="how-it-works-cont" class="container-full">

            <div class="row order" id="ord">
                <div class="col-md-12 text-center">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icon_order.png">
                    <h1 id="chg">Schedule your order</h1>
                    
<form name="contactform" method="post" action="http://www.klin.lu/wp-content/themes/klin/send_form_email.php">
                        <p>
                            <input autocomplete="off" type="text" id="searchCodes" name="zip" onkeyup="myFunction()" placeholder="Enter your ZIP Code…">
                        </p>
                        <p id="result"></p>

                                    <div class="col-md-12 details">
                                        <p>
                                            <input type="text" name="address" placeholder="Street N° and name" class="check"> 
                                            </p>
                                        <p>
                                            <input type="text" name="name" placeholder="Name" class="check"> 
                                            </p>    
                                        <p>
                                            <input type="text" name="surname" placeholder="Surname" class="check"> 
                                            </p>
                                        <p>
                                            <input type="text" name="phone" placeholder="Phone" class="check"> 
                                            </p>
                                        <p>
                                            <input type="text" name="email" placeholder="Email" class="check">
                                        </p>
                                    </div>

                                    <div class="col-md-12 pckcont">
                                        <h3>Pickup date</h3>

                                        <input type="text" placeholder="Choose date" class="datepicker onpkp" name="pckdate">
                            <?php $args = array( 'post_type' => 'zones', 'posts_per_page' => -1 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                <div class="zone <?php echo str_replace('<br />', ' ', get_field('zip_codes')); ?>">


                                        <?php if( have_rows('delivery_pickup') ):
                while ( have_rows('delivery_pickup') ) : the_row(); ?>
                                            <div class="col-md-12 text-center tms-pkp <?php the_sub_field('day'); ?>-pkp">
                                                <h3>Pickup time</h3>

                                                <?php if( have_rows('times') ):
                while ( have_rows('times') ) : the_row(); ?>
                                                 <div class="checkbox pck">
                                                  <input type="checkbox" id="pcktimeid" name="pcktime" value="<?php the_sub_field('from'); ?> - <?php the_sub_field('to'); ?>"><label><?php the_sub_field('from'); ?> - <?php the_sub_field('to'); ?></label>
                                                  </div>
                                                   
                                             

                                                    <?php endwhile; endif; ?>
                                            </div>

                                            <?php endwhile; endif; ?>
                                    </div>
                                                                                                         <?php endwhile; wp_reset_postdata(); ?>
                                    </div>

                                    <div class="col-md-12 delcont">
                                        <h3>Delivery date</h3>

                                        <input type="text" placeholder="Choose date" class="datepickertwo ondel" name="deldate">
                            <?php $args = array( 'post_type' => 'zones', 'posts_per_page' => -1 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                <div class="zone <?php echo str_replace('<br />', ' ', get_field('zip_codes')); ?>">

                                        <?php if( have_rows('delivery_pickup') ):
                while ( have_rows('delivery_pickup') ) : the_row(); ?>
                                            <div class="col-md-12 text-center tms-del <?php the_sub_field('day'); ?>-del">
                                                <h3>Delivery time</h3>

                                                <?php if( have_rows('times') ):
                while ( have_rows('times') ) : the_row(); ?>
                                                    <div class="checkbox del">
                                                   <input type="checkbox" id="deltimeid" name="deltime" value="<?php the_sub_field('from'); ?> - <?php the_sub_field('to'); ?>"><label><?php the_sub_field('from'); ?> - <?php the_sub_field('to'); ?></label>
                                                    </div>

                                                    <?php endwhile; endif; ?>
                                            </div>
                                            <?php endwhile; endif; ?>
                                                                                       
                                    </div>
                                                                                                         <?php endwhile; wp_reset_postdata(); ?>

                                </div>

                                      <div class="col-md-12 lndryshrtscont">                                       
                                      <p><em>Please choose your options if you have shirts or laundry.<br>Your items are sorted and priced according to the list below, everything not listed is laundry priced by the kilo.
                                      
                                      </em></p><br><br>
                                    </div>
                                    <div class="col-md-2"></div>
                                      <div class="col-md-4 shrtscont">
                                        <h3>Shirts</h3>

                                
                                                 <div class="checkbox shrts">
                                                  <input type="checkbox" id="options_one" name="shirts" value="No Shirts">
                                                    <label><img src="<?php echo get_template_directory_uri(); ?>/img/options_no.png"> <span style="padding-top:30px;">No Shirts</span>
                                                     </label>
                                                  </div>
                                                  <div class="checkbox shrts">
                                                  <input type="checkbox" id="options_one" name="shirts" value="Shirts Hung">
                                                    <label><img src="<?php echo get_template_directory_uri(); ?>/img/options_hung.png"> <span>Shirts hung<br><em>From 3€</em></span>
                                                     </label>
                                                  </div>
                                                  <div class="checkbox shrts">
                                                  <input type="checkbox" id="options_one" name="shirts" value="Shirts Folded">
                                                    <label><img src="<?php echo get_template_directory_uri(); ?>/img/options_folded.png"> <span>Shirts folded<br><em>From 5€</em></span>
                                                     </label>
                                                  </div>
                                                  
                                                   
                                             

    
                
                                    </div>
                                    <div class="col-md-4 lndrycont">
                                        <h3>Laundry</h3>

                                
                                                 <div class="checkbox lndry">
                                                  <input type="checkbox" id="options_one" name="laundry" value="No Laundry">
                                                    <label><img src="<?php echo get_template_directory_uri(); ?>/img/options_no.png"> <span style="padding-top:30px;">No Laundry</span>
                                                     </label>
                                                  </div>
                                                  <div class="checkbox lndry">
                                                  <input type="checkbox" id="options_one" name="laundry" value="Wash & Fold">
                                                    <label><img src="<?php echo get_template_directory_uri(); ?>/img/options_wash.png"> <span>Wash & Fold<br><em>3€/kg</em></span>
                                                     </label>
                                                  </div>
                                                  <div class="checkbox lndry">
                                                  <input type="checkbox" id="options_one" name="laundry" value="Iron & Fold">
                                                    <label><img src="<?php echo get_template_directory_uri(); ?>/img/options_iron.png"> <span>Iron & Fold<br><em>5€/kg</em></span>
                                                     </label>
                                                  </div>
                                                  <div class="checkbox lndry">
                                                  <input type="checkbox" id="options_one" name="laundry" value="Wash, Iron & Fold">
                                                    <label><img src="<?php echo get_template_directory_uri(); ?>/img/options_all.png"> <span>Wash, Iron & Fold<br><em>7€/kg</em></span>
                                                     </label>
                                                  </div>
                                                   
                                             

    
                
                                    </div>
                                    <div class="col-md-12 message">
                                    <p><em>Any special needs? leave us your comments or questions below:</em></p><br><br>
                                        <p>
                                            <textarea type="text" name="message"></textarea>
                                            </p>
                                       
                                    </div>
                                    <div class="col-md-12 text-center">
                                    <div class="buttons-one">
                                     <p><button class="navi sched">Next: Schedule</button></p>
                                     </div>
                                     <div class="buttons-two">
                                     <p><button class="navi optprev">Previous: Details</button> <button class="navi opt">Next: Choose options</button></p>
                                     </div>
      <div class="buttons-three">
                                     <p><button class="navi schdprev">Previous: Schedule</button> <button class="navi schd">Next: Finish</button></p>
                                     </div>
                                    </div>
                                    <div class="col-md-12 text-center send">
                                        <button class="navi sendprev">Previous: Options</button>
                                        <input type="submit" value="Submit Order">
                                    </div>

                    </form>
                </div>
            </div>
            <div class="row how" id="how-it-works">
                <div class="col-md-12 text-center">
                    <?php the_field('content_howitworks'); ?>
                </div>
                <?php if( have_rows('step') ):
                while ( have_rows('step') ) : the_row(); ?>
                    <div class="col-md-6 col-lg-3 box">
                        <div class="inner">
                            <img src="<?php the_sub_field('icon'); ?>" alt="">
                            <h3><?php the_sub_field('title'); ?></h3>
                            <p>
                                <?php the_sub_field('content'); ?>
                            </p>
<!--                            <button>Read More</button>-->
                        </div>
                    </div>
                    <?php endwhile; endif; ?>


            </div>

        </section>
        <section id="coverage-map" class="container-full">

            <div class="row">
                <div class="col-md-12 text-center">
                    <?php the_field('content_coveragemap'); ?>
                        

                        <div class="col-md-12 coverage">
                            <div class="row">
                                <div class="col-md-12"><h1>Delivery Zones</h1></div>

                                <?php $args = array( 'post_type' => 'zones', 'posts_per_page' => -1, 'order' => 'asc' );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                    <div class="col-md-3 box">
                                        <div class="inner">
                                            <h4><?php the_field('description'); ?></h4>
                                 
                                        </div>
                                    </div>
                                    <?php endwhile; wp_reset_postdata(); ?>
                            </div>
                        </div>
                        <p><button class="check">Delivery Zones</button></p>
<div id="map" style="width:100%;height:550px;"></div>
    <div id="capture"></div>
    <script>
      var map;
      var src = 'http://www.klin.lu/wp-content/themes/klin/map2.kmz';
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(49.6075838, 6.0658306),
          zoom: 11,
          mapTypeId: 'roadmap',
          styles: [
  {
    "featureType": "administrative.country",
    "elementType": "geometry",
    "stylers": [
      {
        "lightness": 25
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels",
    "stylers": [
      {
        "saturation": -100
      },
      {
        "lightness": 55
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "saturation": -100
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
]
        });

        var kmlLayer = new google.maps.KmlLayer(src, {
          suppressInfoWindows: true,
          preserveViewport: false,
          map: map
        });
        kmlLayer.addListener('click', function(event) {
          var content = event.featureData.infoWindowHtml;
          var testimonial = document.getElementById('capture');
          testimonial.innerHTML = content;
        });
      }
    </script>
    


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkajntZN-TCtNkwr92INAOIIwa04XS6cA&callback=initMap"></script>

                </div>
            </div>
        </section>
        <section id="the-process" class="container-full">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php the_field('content_theprocess'); ?>
                       <div id="fader">
                        <?php 

$images = get_field('photos');

if( $images ): ?>
        <?php foreach( $images as $image ): ?>
                     <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endforeach; ?>
<?php endif; ?>
               </div>
                </div>
            </div>
        </section>
        <?php if(get_field("content_whoweare")) : ?>
            <section id="who-we-are" class="container-full light">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <?php the_field('content_whoweare'); ?>
                    </div>
                    <?php if( have_rows('members') ):
                while ( have_rows('members') ) : the_row(); ?>
                        <div class="col-md-4 box text-center">
                            <div class="inner">
                                <img src="<?php the_sub_field('picture_member'); ?>" alt="">
                                <?php the_sub_field('content_member'); ?>
                            </div>
                        </div>
                        <?php endwhile; endif; ?>


                </div>
            </section>
            <?php endif; ?>
                <section id="pricing" class="container-full lighter">
                    <div class="row">
                        <div class="col-md-10 col-md-push-1 text-center"><?php the_field('content_pricing'); ?></div>
                    </div>
                       <div class="row">
                        <div class="col-md-2 col-lg-2 text-center"></div>
                        <div class="col-md-8 col-lg-8 text-center">
                                  <div class="row" style="margin-top:20px;">
        <div class="col-md-6 box text-left" >
                             <?php $args = array( 'post_type' => 'pricelist', 'posts_per_page' => 4, 'order' => 'asc' );
                                      $counter = 0;
$loop = new WP_Query( $args );

while ( $loop->have_posts() ) : $loop->the_post(); ?>
                     
                                          <div class="inner">
                                   <div class="service">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/plus.png" class="plus"> <h2><?php the_title(); ?></h2>
                                    <img src="<?php the_post_thumbnail_url(); ?>" class="serviceicon">
                                    </div>
                                   <div class="items">
                                     <?php if( have_rows('items') ):
                while ( have_rows('items') ) : the_row(); ?>
                                   <p><?php the_sub_field('item'); ?> <span><?php the_sub_field('price'); ?> €</span></p>
                                    <?php endwhile; endif; ?>
                                   <?php if (get_field('note')) : ?><p class="note"><em><?php the_field('note'); ?></em></p><?php endif; ?>
                                          </div>
                                          </div>
                                        
        
                                    <?php endwhile; wp_reset_postdata(); ?>
                                      </div>
                                              <div class="col-md-6 box text-left" >
                             <?php $args = array( 'post_type' => 'pricelist', 'posts_per_page' => 4, 'offset' => 4, 'order' => 'asc' );
                                      $counter = 0;
$loop = new WP_Query( $args );

while ( $loop->have_posts() ) : $loop->the_post(); ?>
                     
                                          <div class="inner">
                                   <div class="service">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/plus.png" class="plus"> <h2><?php the_title(); ?></h2>
                                    <img src="<?php the_post_thumbnail_url(); ?>" class="serviceicon">
                                    </div>
                                   <div class="items">
                                     <?php if( have_rows('items') ):
                while ( have_rows('items') ) : the_row(); ?>
                                   <p><?php the_sub_field('item'); ?> <span><?php the_sub_field('price'); ?> €</span></p>
                                    <?php endwhile; endif; ?>
                                   <?php if (get_field('note')) : ?><p class="note"><em><?php the_field('note'); ?></em></p><?php endif; ?>
                                          </div>
                                          </div>
                                        
        
                                    <?php endwhile; wp_reset_postdata(); ?>
                                      </div>
                                  </div>
                        </div>
                    </div>
                </section>


              <?php get_footer(); ?>
                <div class="bg" style="background-image:url('<?php echo the_post_thumbnail_url(); ?>');"></div>
                <?php wp_footer(); ?>
</body>

</html>
