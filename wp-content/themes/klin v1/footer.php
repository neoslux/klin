  <footer class="container-fluid darker">
                    <div class="row">
                        <div class="col-md-3 spacer">
                        </div>
                        <div class="col-md-3">
                            <h5>CONTACT US </h5>
                            <h4><a href="tel:<?php the_field('phone'); ?>"><?php the_field('phone'); ?></a><br>
                    <a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></h4>
                        </div>
                        <div class="col-md-3">
                            <h5>Supported by</h5>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/technoport.png" alt=""></div>
                        <div class="col-md-3 socials">
                            <h5>FOLLOW US ON</h5>
                            <?php if (get_field('facebook')) : ?><a href="http://www.facebook.com/<?php the_field('facebook'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social_fb.png" alt=""></a>
                                <?php endif; ?>
                                    <?php if (get_field('instagram')) : ?><a href="http://www.facebook.com/<?php the_field('instagram'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social_ig.png" alt=""></a>
                                        <?php endif; ?>
                                            <?php if (get_field('twitter')) : ?><a href="http://www.facebook.com/<?php the_field('twitter'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social_tw.png" alt=""></a>
                                                <?php endif; ?>
                                                    <?php if (get_field('youtube')) : ?><a href="http://www.facebook.com/<?php the_field('facebyoutubeook'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social_yt.png" alt=""></a>
                                                        <?php endif; ?>
                                                            <?php if (get_field('linkedin')) : ?><a href="http://www.facebook.com/<?php the_field('linkedin'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social_li.png" alt=""></a>
                                                                <?php endif; ?>
                                                                    <?php if (get_field('vimeo')) : ?><a href="http://www.facebook.com/<?php the_field('vimeo'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social_vi.png" alt=""></a>
                                                                        <?php endif; ?>
                        </div>
                    </div>
                </footer>