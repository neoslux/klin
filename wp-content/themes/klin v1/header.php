<div class="desktop">
       <header class="container-full sticky">
        <div class="row">
            <div class="col-md-8">
            <a href="http://www.klin.lu"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" id="logo"></a>
            <nav class="menu">
                    <?php wp_nav_menu(''); ?>
                </nav>
            </div>
            <div class="col-md-4 text-right">
                <nav class="contact">
                   <button class="menuorder light">ORDER</button>
                    
                </nav>

            </div>
        </div>
    </header>
    <header class="container-full">
           <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-8 col-xl-6 text-md-right text-xl-left lft">
                <a href="http://www.klin.lu"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"></a>

                <nav class="menu">
        <?php wp_nav_menu(''); ?>
                </nav>
               </div>
            <div class="col-md-12 col-md-4 col-lg-4 col-xl-6 text-right rght">
                <nav class="contact">
                    <ul>
                        <li>
                            <a href="tel:<?php the_field('phone'); ?>"><?php the_field('phone'); ?></a> | 
                            <a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a>
                        </li>

                    </ul>
                </nav>

            </div>
        </div>
    </header>
    </div>
    <div class="mobile">
       <header class="container-full">
        <div class="row">
            <div class="col-xs-6">
                <a href="http://www.klin.lu"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" id="logom"></a>
            
            </div>
            <div class="col-xs-6 text-right">
             
<div class="menu-btn">MENU</div>
            </div>
        </div>
    </header>
    <nav class="menu">

                   <div class="inner">
                                  <button class="menuorder light">ORDER</button>
                    <?php wp_nav_menu(''); ?>
                            <?php if (get_field('facebook')) : ?><a href="http://www.facebook.com/<?php the_field('facebook'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social_fb.png" alt=""></a>
                                <?php endif; ?>
                                    <?php if (get_field('instagram')) : ?><a href="http://www.facebook.com/<?php the_field('instagram'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social_ig.png" alt=""></a>
                                        <?php endif; ?>
                                            <?php if (get_field('twitter')) : ?><a href="http://www.facebook.com/<?php the_field('twitter'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social_tw.png" alt=""></a>
                                                <?php endif; ?>
                                                    <?php if (get_field('youtube')) : ?><a href="http://www.facebook.com/<?php the_field('facebyoutubeook'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social_yt.png" alt=""></a>
                                                        <?php endif; ?>
                                                            <?php if (get_field('linkedin')) : ?><a href="http://www.facebook.com/<?php the_field('linkedin'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social_li.png" alt=""></a>
                                                                <?php endif; ?>
                                                                    <?php if (get_field('vimeo')) : ?><a href="http://www.facebook.com/<?php the_field('vimeo'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social_vi.png" alt=""></a>
                                                                        <?php endif; ?>
                    </div>
                </nav>
</div>