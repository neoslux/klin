<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
        <script src="https://use.typekit.net/bwu6bjg.js"></script>
        <script>
            try {
                Typekit.load({
                    async: true
                });
            } catch (e) {}

        </script>
        <style>
            h1, h2 { margin:0;}
    </style>
</head>

<body style="background-color:#fff;">

<div class="table" style="background:#fff; width:100%; height:100%; display:table;">
<div class="cell" style="background:#fff; width:100%; padding-top:80px; display:table-cell; vertical-align:top; text-align:center;">
<p><a href="http://www.klin.lu"><img src="img/logo.png" width="150"></a></p>

<?php
if(isset($_POST['email'])) {
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "order@klin.lu";
    $email_watch = "me@cliff.lu";
    $email_subject = "Your email subject line";
 
    function died($error) {
        // your error code can go here
        echo "<p>We are very sorry, but there were error(s) found with the form you submitted.</p>";
        echo "<p>These errors appear below.</p><br /><br />";
        echo $error."<br /><br />";
        echo "<p>Please go back and fix these errors.</p><br /><br />";
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['zip']) ||
        !isset($_POST['address']) ||
        !isset($_POST['name']) ||
        !isset($_POST['surname']) ||
        !isset($_POST['email']) ||
        !isset($_POST['phone']) ||
        !isset($_POST['pckdate']) ||
        !isset($_POST['pcktime']) ||
        !isset($_POST['deldate']) ||
        !isset($_POST['deltime']) ||
//        !isset($_POST['shirts']) ||
//        !isset($_POST['laundry']) ||
        !isset($_POST['message']) ||
//        !isset($_POST['phone']) ||
//        !isset($_POST['comments'])) {
        !isset($_POST['phone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
 
     
 
    $address = $_POST['address']; // required
    $zip = $_POST['zip']; // required
    $name = $_POST['name']; // required
    $surname = $_POST['surname']; // required
    $email_from = $_POST['email']; // required
    $phone = $_POST['phone']; // not required
    $pckdate = $_POST['pckdate']; // required
    $pcktime = $_POST['pcktime']; // required
    $deldate = $_POST['deldate']; // required
    $deltime = $_POST['deltime']; // required
    $shirts = $_POST['shirts']; // required
    $laundry = $_POST['laundry']; // required
    $message = $_POST['message']; // required
//    $comments = $_POST['comments']; // required
 
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$name)) {
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
  }

 
//  if(strlen($comments) < 2) {
//    $error_message .= 'The Comments you entered do not appear to be valid.<br />';
//  }
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
 
     
    function clean_string($string) {
      $bad = array("content-type","to:","href");
      return str_replace($bad,"",$string);
    }
 
     
 
    $email_message = '<html><body style="text-align:center">';
    $email_message .= '<img src="http://www.klin.lu/wp-content/themes/klin/img/logo.png" width="150"><br><br>';
    $email_message .= '<h1 style="#7DABA2">Order Summary</h1>';
    $email_message .= "Name: ".clean_string($surname)." ".clean_string($name)."<br>";
    $email_message .= "Email: ".clean_string($email_from)."<br>";
    $email_message .= "phone: ".clean_string($phone)."<br>";
    $email_message .= "<h3>Address</h3>";
    $email_message .= "Street: ".clean_string($address)."<br>";
    $email_message .= "ZIP Code: ".clean_string($zip)."<br>";
    $email_message .= "<h3>Schedule</h3>";
    $email_message .= "Pickup Date: ".clean_string($pckdate)."<br>";
    $email_message .= "Pickup Time: ".clean_string($pcktime)."<br>";
    $email_message .= "Delivery Date: ".clean_string($deldate)."<br>";
    $email_message .= "Delivery Time: ".clean_string($deltime)."<br>";
    $email_message .= "<h3>Options</h3>";
    $email_message .= "Shirts: ".clean_string($shirts)."<br>";
    $email_message .= "Laundry: ".clean_string($laundry)."<br>";
    $email_message .= "Message: ".clean_string($message)."<br>";
 
// create email headers
$headers = 'From: '.$email_from."\r\n".
'MIME-Version: 1.0' . "\r\n".
'Content-type: text/html; charset=utf-8' . "\r\n".
'CC: '.$email_from."\r\n" .
'BCC: '.$email_watch."\r\n" .
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, "Klin.lu - Order", $email_message, $headers);  
?>
 

 <br><br>
    <h1><?php echo $surname; ?> <?php echo $name; ?>,</h1>
      <h2>Thank you for your order!</h2>            
      <br><br>
       <h4>Order Summary</h4>
    <p>Street: <?php echo $address; ?><br>
    ZIP Code: <?php echo $zip; ?></p><br><br>
    
<p><strong>Contacts</strong><br>
    Email: <?php echo $email_from; ?><br>
    Phone: <?php echo $phone; ?></p><br><br>
    

<p>   <strong>Schedule</strong><br>
    Pickup Date: <?php echo $pckdate; ?><br>
    Pickup Time: <?php echo $pcktime; ?><br>
    Delivery Date: <?php echo $deldate; ?><br>
    Delivery Time: <?php echo $deltime; ?></p><br><br>
<p><strong>Options</strong><br>
    Shirts: <?php echo $shirts; ?><br>
    Laundry: <?php echo $laundry; ?>
    </p><p>
    Message: <?php echo $message; ?>
        
    </p><br><br>
    <p><a href="http://www.klin.lu/">Back to Klin</a></p><br><br>
 </div>
 </div>
                 <?php wp_footer(); ?>
</body>
<?php
 
}
?>